# This script is a "quick install" for programs disseq and mpidisseq
# on a laptop

# usage: 
# ------
# 1 - go into the parent directory where you wish disseq to be installed
# 2 - download quick install.sh (this should have been done if you read this script)
# 3 - type: bash quick_install.sh 

# in case they have not been installed; can be done from anywhere

sudo apt install openmpi-bin
sudo apt install cmake

# clone the git where you are

git clone git@gitlab.inria.fr:biodiversiton/disseq.git

# This will create a new subdirectory disseq with all required files

# go into disseq, and src

cd disseq/src

# not useful for first installation
# rm -rf Build

# required for first intallation
mkdir Build
cd Build

# to compile the sources
cmake ..
make
sudo make install

# The executables are in directory /usr/local/bin: disseq and mpidisseq

# they can be launched from anywhere. This will display the help for each
disseq -h
mpidisseq -h
