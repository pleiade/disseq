#!/usr/bin/env python3

"""
 * Copyright INRAE 2022

This file is part of disseq and mpidisseq.
disseq and mpidisseq are free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.


Dépendences:
  sudo apt install parallel
  sudo apt install python3-h5py
  sudo apt install python3-dna-jellyfish
"""

import sys
import os
import glob
import time
import pymp
import h5py
import numpy as np
import dna_jellyfish as jf

def next_seq(handle):
 myID  = False
 mySeq = ''
 for line in handle:
  line = line.rstrip('\n')
  if line.startswith('>'):
   if myID:
    yield myID,mySeq
   mySeq = ''
   myID = line[1:]
  else:
   mySeq += line
 yield myID,mySeq

def histogram(s):
  km   =  jf.ReadMerFile(s + '.jf')
  kmer = {}
  hist  = []
  for mer, count in km:
    kmer[str(mer)] = count
  for h in all_k:
    try:
      hist.append(kmer[h])
    except KeyError:
      hist.append(0)
  return hist

def distances(h1,h2):
  dis = np.sum(np.abs(np.array(h1) - np.array(h2)))
  return dis

############################################""

if len(sys.argv) == 1 or sys.argv[1] == '-h':
  exit("Usage: jelly_diskm basename k")

complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A' }


basename = sys.argv[1]
k        = sys.argv[2]
try: h5  = sys.argv[3]
except:  h5 = False

seq_ids   = []
seq_words = []
#Explode fasta
with open(basename + '.fas' ,'r') as fas_in:
  for myID, mySeq in next_seq(fas_in):
    mySeq = mySeq.replace('\n','').upper()
    with open(myID + '.jfas', 'w') as fas_out, open(myID + '.rev.jfas' ,'w') as fas_rev_out:
      print(f'>{myID}\n{mySeq}', file=fas_out)
      reverse_complement = "".join(complement.get(base, base) for base in reversed(mySeq))
      print(f'>{myID}_rev\n{reverse_complement}', file=fas_rev_out)
    seq_ids.append(myID)
    seq_words.append(mySeq)

#Run jellyfish
print(f'{2 * len(seq_ids)} reads to jellyfish')
cmd = "parallel 'jellyfish count -m " + k + " -s 100000 -c 1 "
cmd += "{.}.jfas -o {.}.jf; "
cmd += "printf \"%s done\\r\" {#}' ::: *.jfas"
os.system(cmd)
print()


all_k = set()
for f in glob.glob('*.jf'):
  kmer = {}
  km =  jf.ReadMerFile(f)
  for mer, count in km:
    all_k.add(str(mer))

all_k = sorted(all_k)
nkmer = 4**int(k)
rkmer = len(all_k)
print(f'{len(seq_ids)} ids {rkmer} kmers found on {nkmer} => {rkmer / nkmer}')

n    = len(seq_ids)
Dis = pymp.shared.array((n,n),dtype='float32')
total = int( n * (n-1) / 2)
nd = pymp.shared.array((1,),dtype=int)
nd[0] = 0

for i in range(n):
  H1     = histogram(seq_ids[i])
  H1_rev = histogram(seq_ids[i]+'.rev')
  with pymp.Parallel(6) as pj:
    for j in pj.xrange(i+1, n):
      d     = distances(H1,histogram(seq_ids[j]))
      d_rev = distances(H1_rev,histogram(seq_ids[j]))
      d = min(d,d_rev)
      Dis[i,j] = d
      Dis[j,i] = d
      nd[0] += 1
      print(f'{nd[0]} distances on {total} done', flush=True, end='\r')
      
print(f'{total} distances on {total} done') # hum hum nd[0] != total ==> parallelization

if h5:
  with h5py.File(f'{basename}.k{k}.h5', 'w') as f5:
    f5.create_dataset(name='seqid',data=seq_ids, dtype='<S255',
      compression="gzip",compression_opts=9)
    f5.create_dataset(name='word',data=seq_words,dtype='<S4000',
      compression="gzip",compression_opts=9)
    f5.create_dataset(name='distances',data=Dis, dtype='float32',
        compression="gzip", compression_opts=9)
    f5.attrs['isSym']  = "Yes"
    #Attributes
    f5.attrs['m'],f5.attrs['n'] = f5['distances'].shape
    f5.attrs['algo'] = f"# built with diskm\n# counts of kmers with jellyfish\n# k = {k}"
else:
  with open(f'{basename}.k{k}.dis', "w") as dis_h:
    print('SEQ_ID\t' + '\t'.join(seq_ids), file=dis_h)
    for i in range(n):
      print(seq_ids[i] + '\t', end='', file=dis_h)
      l = '\t'.join([ str(i) for i in Dis[i] ])
      print(l,file=dis_h)
      
for f in glob.glob('*.jf*'):
  os.remove(f)

