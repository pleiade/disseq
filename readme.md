# Project Disseq 

This project has been developed for computing distances, exact if edit distances, with heuristics if based on kmer histograms, between all pairs of reads of a NGS sample. This is a first step for supervised or unsupervised clustering of reads in an environmental sample to build OTUs. It makes available two programs for computing pairwise distances between sequences/reads:     

<ul>
<li> program `disseq` : edit distances with Smith-Waterman and/or Needleman-Wunsch algorithm, written in C </li>
<li> program `jelly_diskm` : L1 distance between kmer histograms, written in python
 </ul>   


 As computations for different pairs *(i,j)* are independent, both program have been extended to a distributed version with a map-reduce process, both with MPI. This enables to derive computations for large environmental samples (up to 100,000 reads for `mpi-disseq` and up to 10,000 reads for `mpi-jelly_diskm`). 


 #### input/output files   

 For both programs,    

<ul>
<li> The input file is a `fasta` file </li>
<li> The output file is either a `tsv` file (`csv` with `tab` as delimiters) or a `hdf5` file with array of pairwise distances </li>
</ul>


# Program disseq

**disseq** stands for _distances between sequences_. It is both the name of the project (with a capital D) and the name of a program written in C which computes exact edit distances between reads from global or local alignment score. This section is about the program `disseq`. 


## Install

Here is a detailed stepwise procedure to install `disseq` on one's computer (Linux, Ubuntu distribution): 

\### in case these libraries have not been installed beforehand;   
\### can be done from any directory

sudo apt install openmpi-bin   
sudo apt install cmake   

\### clone the git where you want it to be installed (in $)  
\### This will create a new subdirectory `disseq` with all required files  

$ git clone git@gitlab.inria.fr:biodiversiton/disseq.git

\### go into disseq   

cd src

\### not useful for first installation   

rm -rf Build

\### required for first intallation  

mkdir Build  
cd Build

\### to compile the sources  

cmake ..  
make   
sudo make install   

\### The executables are in directory /usr/local/bin: disseq and mpidisseq   
\### they can be launched from anywhere. This will display the help for each  

disseq -h   
mpidisseq -h   



## What is available   

Two programs are available:

* `disseq:` a C program which runs as stand-alone, and can compute distance matrices between a few thousands of reads 

* `mpidisseq:` a distributed version of disseq with MPI, which can compute distance matrices up to 150 000 reads within one day on a National Computing Center (e.g. a hyperparallel machine like a Blue Gene Q) ; this program scales perfectly with the number of cores.

* ...

## usage

`disseq -ref <...> -query <...> -out <...> -algo <...> -sl <...> -id <...> `

## arguments 

The arguments of `disseq` are given as in the following table. Bold value is default value.

|argument | what it is | values |
|-----------------|:-------------|:---------------:|
| **ref** | reference file | **infile.fas**|
| **query**| query file | (optional) |
| **out**| distance file | **outfile.txt**|
| **algo** | algorithm for alignment score | **sw**,  *nw*|
| **sl**   | sequence length in seq_id   | *yes*, **no**   | 
| **id**   | seq_id  for sequence name | **yes**, *no*   | 
| **h5**   | output in h5 format   | *yes*, **no**   | 



They are briefly explained here:
* **ref** is a fasta file. If no query is given, disseq computes all pairwise distances between sequences in file ref. 
If a query file is given it computes all pairwise distances between sequences in ref and sequences in query.

* **query**: see **ref**

* **out:** the distance file as outcome. It can be returned as an ascii file (if `h5==False`) or a hdf5 file (if `h5==true`). If an ascii file, it is returned as an array, with sequence identifiers as rownames and columns names, and all distances on a row are seperated by a tab `\t`. 

## output 

## notes 

## example 

Data sets for examples can be downloaded form Inrae Dataverse at public addresses:   


| dataset | doi/url | brief description |
|-----------------|:-------------|:---------------|
| **guiana_trees** | doi:10.15454/XSJ079 | trnH-psbA barcodes of 1458 trees in French Guiana|
| **atlas_guyane_lauraceae**| ? | 103 barcodes for trees in Lauraceae family from Guiana_trees sample |
| **Teychan_pelagic**| ? | rbcL barcodes of an environmental sample in Arcachon Bay, mainly diatoms, from Malabar project|   
| **Teychan_benthic**| ? | rbcL barcodes of an environmental sample in Arcachon Bay, mainly diatoms, from Malabar project|   


To run an example:   

<ul>
<li> select a directory on your own computer where the example files will be downloaded </li>
<li> go to the url of the dataset (its doi) and download it in the selected directory </li>
<li> go into this directory </li>
<li> type the command for the distance computation you wish (program, parameters, file names) 
</ul>


Here is a simple example with default values:

<ul>
<li> create directory atlas_guyane_lauraceae on your computer</li>
<li> go to the url ? of atlas_guyane_lauraceae dataset </li>
<li> download it in directory atlas_guyane_lauraceae </li>
<li> go into this directory </li>
<li> type the command `disseq -ref atlas_guyane_lauraceae.fas -out atlas_guyane_lauraceae.sw.dis` </li>
</ul>

This will compute all pairwise distances of sequences in fasta file `atlas_guyane_lauraceae.fas` with Smith-Waterman algorithm (local alignment)
and write the outcome in file `atlas_guyane_lauraceae.sw.dis`.  

# Program jelly_diskm

## How does it work

`jelly_diskm` is a python program for computing distances between sequences or reads from their histogram of kmers. A kmer is a word of length *k* on the alphabet (A,C,G,T). There are \$4^k\$  possible words, and the histogram is the counting of how many times each word appears in the sequence. `jellyfish` is a very efficient program to build those histograms. If *k* is large (say, > 12), all words are not represented. `jellyfish` keeps the counts of those words which are represented only, which is often a vey small fraction of the possible ones. In a second step, the distance between two sequences or reads is computed as the distance between the histograms, with L1 norm. There is a distributed version of the second step in case where the number of sequences or reads is large. This program can handle large kmers (like *k = 24* for example), but not very large number of reads or sequences (up to, say, 5,000).

## usage

`jelly_diskm.py <basename> <k> [h5]`


where:

<ul>
<li> the input file is `<basename>.fas` </li>
<li> the length of the kamers is *k* </li>
<li> if `h5` is given as an argument, the output file will be in `hdf5` format; if not, it will be in `tsv`format  (`csv` with `tab` as delimiters) 
</ul>

## example

<ul> 
<li> Go into the dataset with file `atlas_guyane_lauraceae.fas` </li>
<li> type `jelly_diskm.py atlas_guyane_lauraceae 18`

It will write in same directory a file `atlas_guyane_lauraceae.tsv` with pairwise distances from histograms of 18-mers. 

# Miscalleneous

## Available Datasets

The datasets for both `disseq` and `jelly_diskm` are available on INRAE dataverse from where they can be downloaded.   


The datasets are:

* Pinus\_matK.fas : 95 sequences of Pinus species from litterature

* atlas\_guyane\_trnH.fas : 1498 sequences of trees in French Guiana, at adress https://doi:10.15454/XSJ079

* atlas\_guyane\_lauraceae : 103 sequences from atlas\_guyane\_trnH.fas of the family of Lauraceae (a difficult family for barcoding)


# References

## References for the methods 

#### Edit distances

Gusfield, D. (1997) Algorithms on strings, trees and sequences. Cambridge University Press.

Needleman, S. B., & Wunsch, C. D. (1970). A general method applicable to search for similarities in the amino-­acid sequence of two proteins. *Journal of Molecular Biology*, **48,**443-­453.

Smith, P. D., & Waterman, M. S. (1981). Identification of common molecular subsequences. *Journal of Molecular Biology*, **147,**195-­197.

#### jellyfish

Marcais; G. & Kingsford, K. (2011). A fast, lock-free approach for efficient parallel counting of occurrences of k-mers. *Bioinformatics* **27(6):** 764-770 (first published online January 7, 2011) doi:10.1093/bioinformatics/btr011


## References for the datasets

Caron, H, Molino, J‐F, Sabatier, D, et al., 2019, Chloroplast DNA variation in a hyperdiverse tropical tree community. *Ecol Evol.* **9:** 4897-4905. 
https://doi.org/10.1002/ece3.5096 


Abouabdallah, M. A., Peyrard, N. and Franc, A., 2022. Does clustering of DNA barcodes agree with botanical classification directly at high taxonomic levels? Trees in French Guiana as a case study. *Molecular Ecology Resources*, **00:** 1-16, 
https://doi.org/10.1111/1755-0998.13579

# ID

**Contributors**

* Philippe Chaumeil
* Alain Franc
* Jean-Marc Frigerio
* Florent Pruvost
* Sylvie Therond

**Maintainer:** Jean-Marc Frigerio

**Contact:** <Jean-Marc.Frigerio@inrae.fr>

**started:** April 11th, 2018
**version:** 22.09.02




