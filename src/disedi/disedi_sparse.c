/*
 * File:   main.c
 * Author: Philippe
 *
 * Project started on 21 août 2012, 16:19
 * Copyright INRAE 2022

This file is part of disseq and mpidisseq.
disseq and mpidisseq are free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.


 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "com_param.h"
#include "com_func.h"
#include "disseq_func.h"

/*
 *
 */
int main(int argc, char** argv) {

    int i=0, j=0;

    //===================
    //_____Arguments_____
    //===================
    // default values
    int prLength = 0; // print sequence lengths
    int *ppl = &prLength;
    int prIds = 1; // print additional column and line with Sequence Id
    int* ppi = &prIds;
    char *def_algo = "sw";
    char *algo = NULL;
    algo = def_algo;
    char refFile[MAXFILENAME] = "infile.fas"; // input file
    char queryFile[MAXFILENAME] = "\0";   //input file 2 (query)
    char outFile[MAXFILENAME] = "outfile"; // output file written on disk
    char compmatrixFile[MAXFILENAME] = "\0"; // comparison matrix file
    int verbose = 0;
    int hdf5    = 0;

    load_arguments(argc, argv, refFile, queryFile, outFile, &algo, ppl, ppi, compmatrixFile,
                   &verbose, &hdf5);

    if (verbose)		print_accueil();

    if (strncmp(queryFile, refFile, strlen(queryFile)) == 0)
    {
            queryFile[0] = '\0';
    }
    if (verbose)
            print_arguments(refFile, queryFile, outFile, algo, ppl, ppi, compmatrixFile);

    //======================================
    //______chargement fichier reference____
    //======================================

    //====Ouverture du fichier de seq reference====
    FILE *handleRefFile = NULL;
    if((handleRefFile = fopen(refFile, "r")) == NULL){
       printf("%s !!! Cannot open file !!! \n\n",refFile);
       exit(1);
    }

    //====Pré-screen du fichier de seq reference====
    int nbRefSeq = 0;
    int longerSeq = MAXSEQLEN;
    nbRefSeq = get_nb_seq_ref_file(handleRefFile, &longerSeq);
    if (verbose)    printf("longer sequence : %d\n",longerSeq);

    //====structure de stockage base ref====
    struct Oneseq *refseqlist = NULL;
    refseqlist = malloc((nbRefSeq) * sizeof (struct Oneseq));
    if (refseqlist == NULL) {
        fprintf(stderr, "Allocation impossible refseqlist");
        exit(1);
    }

    //====chargement fichier reference====
    int maxRefSeqid=0;
    int longerRefSeq = 0;
    int *plongerRefSeq = &longerRefSeq;
    maxRefSeqid = load_ref_file(handleRefFile, refseqlist, plongerRefSeq, &longerSeq) - 1;
    if (maxRefSeqid < 0) {
        fprintf(stderr,"Reference file unreadable");
        exit(1);
    }
    if (verbose){
            printf("Number of reference sequences loaded: %d\n", nbRefSeq);
            printf("Longer Reference Sequence: %d pb\n", longerRefSeq);
    }
    if (maxRefSeqid > nbRefSeq) {
        printf("Number of reference seq exceed limit :%d\n", nbRefSeq);
        exit(1);
    }
    fclose(handleRefFile);

    //==============================================
    //______chargement fichier query (optionnel)____
    //==============================================
    int nbQuerySeq = 0;
    struct Oneseq *queryseqlist = NULL;
    int maxQuerySeqid=0;
    int longerQuerySeq = 0;
    int flag_2files = 0;        //flag de présence d'un second fichier

    if(queryFile[0] != 0){
        flag_2files = 1;
        //====Ouverture du fichier de seq ====
        FILE *handleQueryFile = NULL;
        if((handleQueryFile = fopen(queryFile, "r")) == NULL){
           printf("%s !!! Cannot open file !!! \n\n",queryFile);
           exit(1);
        }

        //====Pré-screen du fichier de seq ====
        nbQuerySeq = get_nb_seq_ref_file(handleQueryFile, &longerSeq);

        //====structure de stockage base Que====
        queryseqlist = malloc((nbQuerySeq) * sizeof (struct Oneseq));
        if (queryseqlist == NULL) {
            fprintf(stderr, "Allocation impossible queryseqlist");
            exit(1);
        }

         //====chargement fichier query====
        int *plongerQuerySeq = &longerQuerySeq;
        maxQuerySeqid = load_ref_file(handleQueryFile, queryseqlist, plongerQuerySeq, &longerSeq) - 1;
        if (maxQuerySeqid < 0) {
            fprintf(stderr,"Query file unreadable");
            exit(1);
        }
        if (verbose){
                printf("Number of query sequences loaded: %d\n", nbQuerySeq);
                printf("Longer query Sequence: %d pb\n", longerQuerySeq);
        }

        if (maxQuerySeqid > nbQuerySeq) {
            printf("Number of query seq exceed limit :%d\n", nbQuerySeq);
            exit(1);
        }
        fclose(handleQueryFile);
    }else{
        //---utilisation d'un seul fichier---
        nbQuerySeq = nbRefSeq;
        queryseqlist = refseqlist;
        maxQuerySeqid = maxRefSeqid;
        longerQuerySeq = longerRefSeq;
    }

    //======string d'alignement=======
    char *way1_align = malloc((longerRefSeq + longerQuerySeq) * sizeof(char));
    char *way2_align = malloc((longerRefSeq + longerQuerySeq) * sizeof(char));

    //==========================
    //_____FICHIER DE SORTIE____
    //==========================
    if (hdf5 == 0) {
      FILE *fresu;
      if ((fresu = fopen(outFile, "w")) == NULL) {
          fprintf(stderr, "%s !!! Cannot open file !!! \n\n", outFile);
          exit(1);
      }
      //write header line
      if (strcmp(algo, "nw") == 0){fprintf(fresu,"#format_nw\talgo:Needleman-Wunsh\t");}
      if (strcmp(algo, "nwt") == 0){fprintf(fresu,"#format_nwt\talgo:Needleman-Wunsh-traceback\t");}
      if (strcmp(algo, "sw") == 0){fprintf(fresu,"#format_sw\talgo:smith-waterman\t");}
      if (strcmp(algo, "swxx") == 0){fprintf(fresu,"#format_swxx\talgo:smith-waterman\t");}
      if (strcmp(algo, "swt") == 0){fprintf(fresu,"#format_swt\talgo:smith-waterman-traceback\t");}
      fprintf(fresu,"nb_ref_seq:%d\tlonger_ref_seq:%d\n",nbRefSeq,longerRefSeq);
      fprintf(fresu,"###Ref seq File Name : %s\n", refFile);
      if(queryFile[0] != '\0'){fprintf(fresu,"###Query seq File Name: %s\n", queryFile);}
      if ((strcmp(algo, "nw") == 0)||(strcmp(algo, "nwt") == 0)){fprintf(fresu,"###matrix return distance value as computed by algo nw\n");}
      if ((strcmp(algo, "sw") == 0)||(strcmp(algo, "swxx") == 0)||(strcmp(algo, "swt") == 0)){fprintf(fresu,"###matrix return for each pair of sequence the distance computed as ((shorter sequence length - SW score) / 2.0)\n");}
      fclose(fresu);
    }

    //============================
    //_____allocation matrices____
    //============================
        //création de la matrice resu
    float **MRESU;
    MRESU = alloc_RESU_matrix(nbQuerySeq, nbRefSeq);

        //création de la matrice NW/SW
    float **NW = NULL;
    NW = alloc_NW_matrix(longerSeq);
    float **SWrev = NULL;

        //création matrice comp_Matrix et traceback
    float **comp_Matrix = NULL;        //comparison matrix
    float **TBK;
    TBK = alloc_NW_matrix(0);   //évite les warnings de GCC
    float **TBKrev;
    TBKrev = alloc_NW_matrix(0);   //évite les warnings de GCC

        //___chargement matrice comparaison
    if(compmatrixFile[0] != 0){

        if (verbose) printf("request loading comp matrix\n");
        FILE *handleMatrixFile = NULL;
        if((handleMatrixFile = fopen(compmatrixFile, "r")) == NULL){
           printf("%s !!! Cannot open file !!! \n\n",compmatrixFile);
           exit(1);
        }
        comp_Matrix = load_comp_matrix(handleMatrixFile);
    }else{
        if (verbose) printf("request loading default comp matrix\n");
        comp_Matrix = build_comp_matrix();
    }

    if (strcmp(algo, "nwt") == 0){
        fill_penality_comp_matrix(comp_Matrix);
        //création matrice traceback
        TBK = alloc_NW_matrix(longerSeq);
        TBKrev = alloc_NW_matrix(longerSeq);
    }
    if (strcmp(algo, "swt") == 0){
        //création seconde matrice SW
        SWrev = alloc_NW_matrix(longerSeq);
        //création matrice traceback
        TBK = alloc_NW_matrix(longerSeq);
        TBKrev = alloc_NW_matrix(longerSeq);
    }
    if(compmatrixFile[0] != 0){
        if (verbose){
                                        printf("print loaded comp matrix\n");
                                        print_comp_matrix_fileload(comp_Matrix);
                                }
    }else{
        if (verbose){
                                        printf("default comp matrix\n");
                                        print_comp_matrix(comp_Matrix);
                                }
    }

    //=============================
    //_____CALCUL DES DISTANCES____
    //=============================
    //barre progression
    printf("\ncomputing : \n");
    float nb_calcul;
    if(flag_2files == 0){
        nb_calcul = (nbRefSeq * (nbRefSeq - 1)) / 2;
    }else{
        nb_calcul = nbRefSeq * nbQuerySeq;
    }
    printf("nb operation to do : %.0f\n", nb_calcul);
    int bloc_calcul = 100;
    int bloc_progress = 0;
    int calcul_done = 0;

    FILE *fresu;
      if ((fresu = fopen(outFile, "w")) == NULL) {
          fprintf(stderr, "%s !!! Cannot open file !!! \n\n", outFile);
          exit(1);
      }

    //bouclage sur sequences
    float my_dist = 0;
    int j_start = 0;
    for(i = 0; i <= maxQuerySeqid; i++){
        if(flag_2files == 0){j_start = i + 1;}
        for(j = j_start; j <= maxRefSeqid; j++){
            if (strcmp(algo, "nw") == 0) {
                my_dist = nw_distance(queryseqlist[i], refseqlist[j], NW);
            }else if (strcmp(algo, "nwt") == 0) {
                my_dist = nw_traceback(queryseqlist[i], refseqlist[j], NW, TBK, TBKrev, comp_Matrix, way1_align, way2_align);
            }else if (strcmp(algo, "sw") == 0) {
                my_dist = sw_distance(queryseqlist[i], refseqlist[j], NW, comp_Matrix);
                if(my_dist <= 30)
                {
                  fprintf(fresu,"%s\t%s\t%.2f\n",queryseqlist[i].id + 1, refseqlist[j].id + 1, my_dist);
                }
            }else if (strcmp(algo, "swxx") == 0) {
                my_dist = sw_distance_txtx(queryseqlist[i], refseqlist[j], NW, comp_Matrix);
            }else if (strcmp(algo, "swt") == 0){
                my_dist = sw_traceback(queryseqlist[i], refseqlist[j], NW, SWrev, TBK, TBKrev, comp_Matrix, way1_align, way2_align);
            }else{
                fprintf(stderr,"Algorithm unknown\n");
                exit(1);
            }
            progress_bar(bloc_calcul, &bloc_progress, &calcul_done);
            MRESU[i][j] = my_dist;
            if(flag_2files == 0){
               MRESU[j][i] = my_dist;
            }
        }
    }
    printf("\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r%d done\n", calcul_done);

    //=========================
    //_____SORTIE RESULTATS____
    //=========================

    if (hdf5 != 0)
    {  
      /* Need to store the matrix data as a vector (row major) for hdf5 */
      float *VRESU = (float *)malloc(nbQuerySeq*nbRefSeq*sizeof(float));
      for (int i=0; i<nbQuerySeq; i++){
          for (int j=0; j<nbRefSeq; j++){
              VRESU[i*nbRefSeq + j] = MRESU[i][j];
          }
      }
      /* need a char* array for hdf5 write interface */
      char *seqidqueryh5[nbQuerySeq];
      char *wordqueryh5[nbQuerySeq];
      if(flag_2files == 1){
        for (int i=0; i<nbQuerySeq; i++){
            seqidqueryh5[i] = (char *)malloc(MAXIDLEN*sizeof(char));
            strcpy(seqidqueryh5[i], queryseqlist[i].id + 1);
            wordqueryh5[i] = (char *)malloc(MAXSEQLEN*sizeof(char));
            strcpy(wordqueryh5[i], queryseqlist[i].dna);
        }
      }     

      char *seqidrefh5[nbRefSeq];
      char *wordrefh5[nbRefSeq];

      for (int i=0; i<nbRefSeq; i++){
          seqidrefh5[i] = (char *)malloc(MAXIDLEN*sizeof(char));
          strcpy(seqidrefh5[i], refseqlist[i].id + 1);
          wordrefh5[i] = (char *)malloc(MAXSEQLEN*sizeof(char));
          strcpy(wordrefh5[i], refseqlist[i].dna);
      }
      /* output file name for hdf5 */
      char outFileH5[MAXFILENAME] = "";
      writeHdf5(nbQuerySeq, nbRefSeq, nbRefSeq, 0, VRESU, seqidrefh5, NULL, wordrefh5, NULL, outFile);
      /* free our data stored in a vector */
      free(VRESU);
      /* free memory char* array */
      if(flag_2files == 1){
        for (int i=0; i<nbQuerySeq; i++){
            free(seqidqueryh5[i]);
            free(wordqueryh5[i]);
        }
      }
      for (int i=0; i<nbRefSeq; i++){
          free(seqidrefh5[i]);
          free(wordrefh5[i]);
      }
    } else 
      //writeEDmat(MRESU, maxQuerySeqid, maxRefSeqid, outFile, ppi, ppl, queryseqlist, refseqlist);

    //========================================
    //__________ libération mémoire___________
    //========================================

    free_refseqlist(refseqlist, nbRefSeq);
    if (flag_2files == 1){
      free_refseqlist(queryseqlist, nbQuerySeq);
    }
    free_RESU_matrix(MRESU, nbQuerySeq, nbRefSeq);
    free_NW_matrix(NW, longerSeq);
    free_comp_matrix(comp_Matrix);
    if ((strcmp(algo, "nwt") == 0) || (strcmp(algo, "swt") == 0)){
        free_NW_matrix(TBK, longerSeq);
        free_NW_matrix(TBKrev, longerSeq);
    }
    if (strcmp(algo, "swt") == 0){
        free_NW_matrix(SWrev, longerSeq);
    }
    return (EXIT_SUCCESS);
}
