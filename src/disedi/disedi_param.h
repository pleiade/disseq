/* 
 * File:   disseq_param.h
 * Author: Philippe
 *
 * Created on 21 août 2012, 16:49
 *
 * Copyright INRAE 2022

This file is part of disseq and mpidisseq.
disseq and mpidisseq are free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 */

#ifndef DISSEQ_PARAM_H
#define	DISSEQ_PARAM_H


#endif	/* DISSEQ_PARAM_H */
