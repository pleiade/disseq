/*
 * Copyright INRAE 2022

This file is part of disseq.
disseq is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../Common_func/com_param.h"

void print_accueil() {
    printf("******************************** \n");
    printf("           Disedi                \n");
    printf("******************************** \n\n");
    printf("Arguments list:\n");
    printf("\t-ref\t(infile name)\n");
    printf("\t-query\t(optional infile 2)\n");
    printf("\t-out\t(outfile name)\n");
    printf("\t-algo\t(nw/sw/swxx/nwt/swt)\n");
    printf("\t-sl\t(to print sequence lengths yes/no)\n");
    printf("\t-id\t(to print sequence Id yes/no)\n");
    printf("\t-cmpx\t(optional comp matrix file name)\n");
    printf("\t-h5 or -hdf5\t(optional output file in hdf5 format)\n");
		printf("\t-h\t(print this help and exit)\n");
    printf("Parameters list:\n");
    printf("\tMaximum number of characters for a filename :%d\n", MAXFILENAME);
    printf("\tMaximum number of characters for a line :%d\n", MAXLINE);
    printf("\tMaximum number of characters for an identifier :%d\n", MAXIDLEN);
    printf("\tDefault Sequence maximum length :%d\n", MAXSEQLEN);
    printf("_____________ START ____________\n\n");
}

int load_arguments(int argc, char **argv, char *refFile, char *queryFile, char *outFile, char **ppalgo, int *prLength, int *prIds, char *compmatrixFile, int *verbose, int * hdf5) {
    /*=================================================================
          Arguments
                  -ref	infile		filename for input (i.e. input file vs input file)
                  -query  infile2       second optional input file for query vs ref computation
                  -out	outfile		filename for writing result
                  -algo nw/sw/nwt/swt   choice of algorithm
                  -sl     yes/no        print sequence length
                  -id     yes/no        add a column and a line with sequence ids
     ===================================================================*/
    int a = 0;
    for (a = 1; a < argc; a++) {
        if (strcmp(argv[a], "-ref") == 0) {
            if (strlen(argv[a + 1]) > MAXFILENAME) {
                printf("Name of sequence File exceed limit :%d characters\n", MAXFILENAME);
                exit(1);
            }
            strcpy(refFile, argv[a + 1]);
            a++;
        }
        if (strcmp(argv[a], "-query") == 0) {
            if (strlen(argv[a + 1]) > MAXFILENAME) {
                printf("Name of sequence File exceed limit :%d characters\n", MAXFILENAME);
                exit(1);
            }
            strcpy(queryFile, argv[a + 1]);
            a++;
        }
        if (strcmp(argv[a], "-out") == 0) {
            if (strlen(argv[a + 1]) > MAXFILENAME) {
                printf("Name of output File exceed limit :%d characters\n", MAXFILENAME);
                exit(1);
            }
            strcpy(outFile, argv[a + 1]);
            a++;
        }
        if (strcmp(argv[a], "-algo") == 0) {
            *ppalgo = argv[a + 1];
            a++;
        }
        if (strcmp(argv[a], "-sl") == 0) {
            if(strcmp(argv[a + 1], "yes") !=0){
                *prLength = 0;
            }
            a++;
        }
        if (strcmp(argv[a], "-id") == 0) {
            if(strcmp(argv[a + 1], "yes") != 0){
                *prIds = 0;
            }
            a++;
        }
        if (strcmp(argv[a], "-cmpx") == 0) {
            if(strlen(argv[a + 1]) > MAXFILENAME){
                printf("Name of comparison matrix File exceed limit :%d characters\n", MAXFILENAME);
                exit(1);
            }
            printf("WARNING ! Please check you have included cost for indel (-) and undefined (_) in your comparison matrix file!\n");
            strcpy(compmatrixFile, argv[a + 1]);
            a++;
        }
				if (strcmp(argv[a], "-h") == 0) {
					*verbose = 1;
				}
        if (strcmp(argv[a], "-h5") == 0 || strcmp(argv[a], "-hdf5") == 0) {
					*hdf5 = 1;
				}
    }
    return 0;
}

void print_arguments(char *refFile, char *queryFile, char *outFile, char *algo, int *prLength, int *prIds, char *compmatrixFile) {
    printf("*Arguments :\n");
    printf("\tinfile name : %s \n", refFile);
    if(queryFile[0] != 0){printf("\tinfile2 name : %s\n", queryFile);}
    printf("\toutfile name : %s \n", outFile);
    if(compmatrixFile[0] != 0){
        printf("\tcomparison matrix file name : %s\n", compmatrixFile);
    }else{
        if (strcmp(algo, "swxx") == 0){
            fprintf(stderr, "error : for algo swxx, a comparison matrix file is required!\n");
        }
    }
    if (*prLength == 1) {
        printf("\tprint sequence lengths ? yes\n");
    } else {
        printf("\tprint sequence lengths ? no\n");
    }
    if (*prIds == 1) {
        printf("\tprint sequence Id ? yes\n");
    } else {
        printf("\tprint sequence Id no\n");
    }
    printf("\tchoice algo ? %s\n", algo);
    printf("________________________________ \n\n");
}


