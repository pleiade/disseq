/*
 * File:   com_func.h
 * Author: Philippe
 *
 * Created on 21 août 2012, 11:59
 * Modified 20 feb 2019
 * Copyright INRAE 2022

This file is part of disseq.
disseq is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */

#ifndef COM_FUNC_H
#define	COM_FUNC_H

void my_chomp(char *myChaine);
int findMin(int x1, int x2, int x3);
void convertToUpperCase(char *sPtr);
unsigned long split_chaine_to_int_array(char *myChaine, unsigned long **pntToIntArray);
char* split_first_item(char *MyChaine);
float **convert_to_freq_array(unsigned long **intArray, size_t line_number, size_t nb_item);
char complement_nucl(char mon_nucl);
char get_aa(char *mystring);
char* translate_dnaseq(char *madnaseq);

//====Chargement====
int get_nb_seq_ref_file(FILE *fp, int *longerSeq);
int load_ref_file(FILE *fp, struct Oneseq *refseqlist, int *plongerRefSeq, int *longerSeq);
int read_sequence(FILE *fp, struct Oneseq *myseq, int *plongerSeq, int *MaxSeqLength);
void free_refseqlist(struct Oneseq *refseqlist, int nbRefSeq);

//====Stockage Resultat====
float **alloc_RESU_half_matrix(size_t nbSeqij);
void free_RESU_half_matrix(float **MRESU, size_t nbSeqi);
float **alloc_RESU_matrix(size_t nbSeqi, size_t nbSeqj);
void free_RESU_matrix(float **MRESU, size_t nbSeqi, size_t nbSeqj);

//====Espace calcul====
float **alloc_NW_matrix(int maxseqlen);
void free_NW_matrix(float **NW, int maxseqlen);
void print_NW_matrix(float **NW, int maxseqlen);
int init_SW_matrix(float **SW, int maxseqlen);

//====Matrice de Score====
float **build_comp_matrix();
float **load_comp_matrix(FILE *fp);
void free_comp_matrix(float **compMatrix);
void print_comp_matrix(float **compMatrix);
void print_comp_matrix_fileload(float **compMatrix);
int invalid_char(char *my_char, float **compMatrix);
int invalid_string(char *my_string, float **compMatrix);
void fill_penality_comp_matrix(float **compMatrix);

//====ALGORITHMES====
void reverse_complement(struct Oneseq stru);
int matchexact(struct Oneseq stru, struct Oneseq struL);
float nw_distance(struct Oneseq stru1, struct Oneseq stru2, float **NW);
float nw_traceback(struct Oneseq stru1, struct Oneseq stru2, float **NW, float **TBK, float **TBKrev, float **comp_matrix, char *stru1_align, char *stru2_align);
float max_score_seq(char *string, float **comp_matrix);
float sw_compute(char *string1, char *string2, float **SW, float **comp_matrix);
float sw_distance_txtx(struct Oneseq stru1, struct Oneseq stru2, float **SW, float **comp_matrix);
float sw_distance(struct Oneseq stru1, struct Oneseq stru2, float **SW, float **comp_matrix);
float sw_traceback(struct Oneseq stru1, struct Oneseq stru2, float **SW, float **SWrev, float **TBK, float **TBKrev, float **comp_matrix, char *stru1_align, char *stru2_align);
float dis_L1(float *histo1, float *histo2, unsigned long nb_item);

//====Progression & Sortie Résultat====
void writeEDmat(float **MRESU, int maxseqi, int maxseqj, char outfile[], int *prIds, int *prLength, struct Oneseq *seqlisti, struct Oneseq *seqlistj);
void writeHdf5(int nrows, int ncols, int nrowsmpi, int offsetmpi, float *distances, char **seqidqueryh5, char **seqidrefh5, char **wordqueryh5, char **wordrefh5, char outfile[]);

void progress_bar(int bloc_calcul, int *bloc_progress, int *calcul_done);

#endif	/* COM_FUNC_H */
