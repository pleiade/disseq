/*
 *Copyright INRAE 2022

This file is part of disseq.
disseq  is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <hdf5.h>
#ifdef MPI
#include <mpi.h>
#endif
//#include <vector>
#include "com_param.h"
#include "com_int_param.h"

void my_chomp(char *myChaine){
    if(myChaine[strlen(myChaine)-1] == '\n'){
        if(myChaine[strlen(myChaine)-2] == '\r'){
            myChaine[strlen(myChaine)-2] = '\0';
        }else{
            myChaine[strlen(myChaine)-1] = '\0';
        }
    }
}

int findMin(int x1, int x2, int x3) {
    /*_find minimum of three values _*/
    int min = x1;
    if (x2 < x1) {
        min = x2;
    }
    if (x3 < min) {
        min = x3;
    }
    return min;
}

void convertToUpperCase(char *sPtr) {
    while(*sPtr != '\0'){
        *sPtr = toupper((unsigned char)*sPtr);
        sPtr++;
    }
}

char* split_first_item(char *MyChaine){
    //read a tab separated chain
    //keep first item (!!modify initial chain!!)
    //return rest of the chain
    char *saveptr;
    const char d[2] = "\t";
    /* keep first item */
    strtok_r(MyChaine, d, &saveptr);

    return saveptr;
}

unsigned long split_chaine_to_int_array(char *myChaine, unsigned long **pntToIntArray){
    //read a tab separated txt file and store in an array of unsigned long int
    //return number of item stored
    size_t myAlloc = 256;
    int AllocSet = 1;
    unsigned long i = 0;
    unsigned long *myIntArray;
    myIntArray = calloc(myAlloc, sizeof (unsigned long));
    if (myIntArray == NULL) {
        fprintf(stderr, "Allocation error myIntArray");
        exit(1);
    }
    unsigned long ret;
    const char d[2] = "\t";
    char *saveptr;
    char *token;

    /* initialise token */
    token = strtok_r(myChaine, d, &saveptr);

    /* get tokens */
    while( token != NULL ) {
        ret = strtoul(token, NULL, 10);
        //TO DO: test valeur de retour
        if(i > (myAlloc * AllocSet)){
            AllocSet++;
            myIntArray = realloc(myIntArray,AllocSet * myAlloc * sizeof (unsigned long));
            if (myIntArray == NULL) {
                fprintf(stderr, "Allocation error myIntArray");
                exit(1);
            }
        }
        myIntArray[i] = ret;
        i++;
        token = strtok_r(NULL, d, &saveptr);
    }
    *pntToIntArray = myIntArray;
    return i;
}

float **convert_to_freq_array(unsigned long **intArray, size_t line_number, size_t nb_item){
    /* take an array of unsigned long with line_number line and nb_item column*/
    /* return an array of float whith frequencies computed line per line*/
    float **freqArray;
    freqArray = calloc(line_number, sizeof(float *));
    if (freqArray == NULL) {
        fprintf(stderr, "Allocation error freqArray\n");
        exit(1);
    }
    int i;
    int j;
    for (i = 0; i < line_number; i++){
        freqArray[i] = calloc(nb_item, sizeof(float));
        if (freqArray[i] == NULL) {
            fprintf(stderr, "Allocation error freqArray[%d]\n", i);
            exit(1);
        }
    }
    /*fill matrix*/
    unsigned long sum;
    for (i = 0; i < line_number; i++){
        sum = 0;
        for (j = 0; j < nb_item; j++){
            sum = sum + intArray[i][j];
        }
        for (j = 0; j < nb_item; j++){
            freqArray[i][j] = (float) intArray[i][j] / sum;
        }
    }

    return freqArray;
}

char complement_nucl(char mon_nucl) {
    switch (mon_nucl) {
        case 'A':
            return 'T';
            break;
        case 'a':
            return 't';
            break;
        case 'T':
            return 'A';
            break;
        case 't':
            return 'a';
            break;
        case 'G':
            return 'C';
            break;
        case 'g':
            return 'c';
            break;
        case 'C':
            return 'G';
            break;
        case 'c':
            return 'g';
            break;
        case '-':
            return '-';
            break;
        case '_':
            return '_';
            break;
        case '?':
            return '?';
            break;
        default :
            return 'N';
            break;
    }
}

void reverse_complement(struct Oneseq stru) {
    int i = 0;
    stru.rdna[0] = '\0';
    for (i = 0; i < stru.len ; i++) {
        stru.rdna[i] = complement_nucl(stru.dna [stru.len - i - 1]);
    }
    stru.rdna[i] = '\0';
}

char get_aa(char *mystring){
    //return corresponding amino acid for nucleotide triplet starting at given char pointer argument

      if(mystring){
              if(strncmp(mystring,"TC",2) == 0){return 'S';}
              //else if(strncmp(mystring,"TCA",3) == 0){return 'S';}
              //else if(strncmp(mystring,"TCC",3) == 0){return 'S';}
              //else if(strncmp(mystring,"TCG",3) == 0){return 'S';}
              //else if(strncmp(mystring,"TCT",3) == 0){return 'S';}
              else if(strncmp(mystring,"TTC",3) == 0){return 'F';}
              else if(strncmp(mystring,"TTT",3) == 0){return 'F';}
              else if(strncmp(mystring,"TTA",3) == 0){return 'L';}
              else if(strncmp(mystring,"TTG",3) == 0){return 'L';}
              else if(strncmp(mystring,"TAC",3) == 0){return 'Y';}
              else if(strncmp(mystring,"TAT",3) == 0){return 'Y';}
              else if(strncmp(mystring,"TAA",3) == 0){return '_';}
              else if(strncmp(mystring,"TAG",3) == 0){return '_';}
              else if(strncmp(mystring,"TGC",3) == 0){return 'C';}
              else if(strncmp(mystring,"TGT",3) == 0){return 'C';}
              else if(strncmp(mystring,"TGA",3) == 0){return '_';}
              else if(strncmp(mystring,"TGG",3) == 0){return 'W';}
              else if(strncmp(mystring,"CT",2) == 0){return 'L';}
              //else if(strncmp(mystring,"CTA",3) == 0){return 'L';}
              //else if(strncmp(mystring,"CTC",3) == 0){return 'L';}
              //else if(strncmp(mystring,"CTG",3) == 0){return 'L';}
              //else if(strncmp(mystring,"CTT",3) == 0){return 'L';}
              else if(strncmp(mystring,"CC",2) == 0){return 'P';}
              //else if(strncmp(mystring,"CCA",3) == 0){return 'P';}
              //else if(strncmp(mystring,"CCC",3) == 0){return 'P';}
              //else if(strncmp(mystring,"CCG",3) == 0){return 'P';}
              //else if(strncmp(mystring,"CCT",3) == 0){return 'P';}
              else if(strncmp(mystring,"CAC",3) == 0){return 'H';}
              else if(strncmp(mystring,"CAT",3) == 0){return 'H';}
              else if(strncmp(mystring,"CAA",3) == 0){return 'Q';}
              else if(strncmp(mystring,"CAG",3) == 0){return 'Q';}
              else if(strncmp(mystring,"CG",2) == 0){return 'R';}
              //else if(strncmp(mystring,"CGA",3) == 0){return 'R';}
              //else if(strncmp(mystring,"CGC",3) == 0){return 'R';}
              //else if(strncmp(mystring,"CGG",3) == 0){return 'R';}
              //else if(strncmp(mystring,"CGT",3) == 0){return 'R';}
              else if(strncmp(mystring,"ATG",3) == 0){return 'M';}
              else if(strncmp(mystring,"AT",2) == 0){return 'I';}
              //else if(strncmp(mystring,"ATA",3) == 0){return 'I';}
              //else if(strncmp(mystring,"ATC",3) == 0){return 'I';}
              //else if(strncmp(mystring,"ATT",3) == 0){return 'I';}
              else if(strncmp(mystring,"AC",2) == 0){return 'T';}
              //else if(strncmp(mystring,"ACA",3) == 0){return 'T';}
              //else if(strncmp(mystring,"ACC",3) == 0){return 'T';}
              //else if(strncmp(mystring,"ACG",3) == 0){return 'T';}
              //else if(strncmp(mystring,"ACT",3) == 0){return 'T';}
              else if(strncmp(mystring,"AAC",3) == 0){return 'N';}
              else if(strncmp(mystring,"AAT",3) == 0){return 'N';}
              else if(strncmp(mystring,"AAA",3) == 0){return 'K';}
              else if(strncmp(mystring,"AAG",3) == 0){return 'K';}
              else if(strncmp(mystring,"AGC",3) == 0){return 'S';}
              else if(strncmp(mystring,"AGT",3) == 0){return 'S';}
              else if(strncmp(mystring,"AGA",3) == 0){return 'R';}
              else if(strncmp(mystring,"AGG",3) == 0){return 'R';}
              else if(strncmp(mystring,"GT",2) == 0){return 'V';}
              //else if(strncmp(mystring,"GTA",3) == 0){return 'V';}
              //else if(strncmp(mystring,"GTC",3) == 0){return 'V';}
              //else if(strncmp(mystring,"GTG",3) == 0){return 'V';}
              //else if(strncmp(mystring,"GTT",3) == 0){return 'V';}
              else if(strncmp(mystring,"GC",2) == 0){return 'A';}
              //else if(strncmp(mystring,"GCA",3) == 0){return 'A';}
              //else if(strncmp(mystring,"GCC",3) == 0){return 'A';}
              //else if(strncmp(mystring,"GCG",3) == 0){return 'A';}
              //else if(strncmp(mystring,"GCT",3) == 0){return 'A';}
              else if(strncmp(mystring,"GAC",3) == 0){return 'D';}
              else if(strncmp(mystring,"GAT",3) == 0){return 'D';}
              else if(strncmp(mystring,"GAA",3) == 0){return 'E';}
              else if(strncmp(mystring,"GAG",3) == 0){return 'E';}
              else if(strncmp(mystring,"GG",2) == 0){return 'G';}
              //else if(strncmp(mystring,"GGA",3) == 0){return 'G';}
              //else if(strncmp(mystring,"GGC",3) == 0){return 'G';}
              //else if(strncmp(mystring,"GGG",3) == 0){return 'G';}
              //else if(strncmp(mystring,"GGT",3) == 0){return 'G';}
              else{
                  fprintf(stderr,"unknown codon : (%.3s)\n",mystring);
                  return '_';
              }
      }
      return '\0';
}

char* translate_dnaseq(char *myDnaSeq){
    //return a pointer on a string containing aminoacid seq for nucleic acid ses given in a string argument
    if(myDnaSeq == NULL){return NULL;}
    //allocation string
    int nb_aa = 0;
    int nb_nuc = strlen(myDnaSeq);
    nb_aa = (nb_nuc/3);
    char *translated = NULL;
    translated = malloc((nb_aa + 1) * sizeof(char));
    if(translated == NULL){
        fprintf(stderr,"Allocation not possible translated dnaseq");
        exit(1);
    }
    //calcul traduction
    int i;
    int pos = 0;
    translated[0] = '\0';
    for (i = 0; i < nb_aa ; i++) {
        translated[i] = get_aa(myDnaSeq + pos);
        pos += 3;
    }
    translated[i] = '\0';
    return translated;
}
//=====================Chargement=====================

int get_nb_seq_ref_file(FILE *fp, int *longerSeq){
    char testligne[MAXLINE];
    int nbRefSeq = 0;
    int cptligne = 0;
    while(fgets(testligne, MAXLINE, fp) != NULL){
        cptligne++;
        //--- check line length---
        if(strlen(testligne) >= (MAXLINE-1)){
            fprintf(stderr, "Length of line %d in the file (%d) exceed limit of %d\n", cptligne, (int)strlen(testligne), MAXLINE);
            fprintf(stderr, "Line in reference file too long\n");
            exit(1);
        }
        if(strlen(testligne) > *longerSeq){
            *longerSeq = strlen(testligne);
        }
        if(testligne[0]=='>'){
            nbRefSeq++;
        }
    }
    fseek ( fp , 0 , SEEK_SET );    //rembobinage début de fichier
    return nbRefSeq;
}

int load_ref_file(FILE *fp, struct Oneseq *refseqlist, int *plongerRefSeq, int *longerSeq){
    // read a fasta file containing references sequences and store them in a Oneseq structure
    //  return total number of sequences read

   int  i = 0, cs = 0, intTemp = 0;
   char tempseq[*longerSeq];  //concatenation buffer
   char ligne[MAXLINE];

    //==============================
    //_____read reference file______
    //==============================
   tempseq[0] = '\0'	; //buffer initialization

   for(i=0; fgets(ligne, MAXLINE, fp); ++i){
        //--- check line length---
        if(strlen(ligne) >= (MAXLINE-1)){
            fprintf(stderr, "Length of line %d in the reference file (%d) exceed limit of %d\n", i, (int)strlen(ligne), MAXLINE);
            fprintf(stderr, "Line in reference file too long\n");
            exit(1);
        }
        my_chomp(ligne);
       if(ligne[0]=='>'){  //description line
           if(i>0){    //except first line
               refseqlist[cs].dna = malloc((strlen(tempseq)+1) * sizeof(char));
               if(refseqlist[cs].dna == NULL){
                    fprintf(stderr,"Allocation not possible refseqlist[cs].dna");
                    exit(1);
                }
               strcpy(refseqlist[cs].dna, tempseq);

               refseqlist[cs].len = strlen(tempseq);
               if(refseqlist[cs].len > *plongerRefSeq){
                   *plongerRefSeq = refseqlist[cs].len;
               }

               refseqlist[cs].rdna = malloc((strlen(tempseq)+1) * sizeof(char));
               if(refseqlist[cs].rdna == NULL){
                    fprintf(stderr,"Allocation not possible refseqlist[cs].rdna");
                    exit(1);
                }
               reverse_complement(refseqlist[cs]);

               tempseq[0] = '\0';
               cs++;
           }
           refseqlist[cs].id = malloc((strlen(ligne)+1)*sizeof(char));
           if(refseqlist[cs].id == NULL){
                fprintf(stderr,"Allocation not possible refseqlist[cs].id");
                exit(1);
            }
           strcpy(refseqlist[cs].id, ligne);
       }else{   //read line of sequence
            intTemp = 0;
            intTemp += strlen(tempseq);
            intTemp += strlen(ligne); //note : direct sum of 2 strlen on the same line failed!!
            if(intTemp > *longerSeq){
                fprintf(stderr, "Reference sequence too long");
                exit(1);
            }
           strcat(tempseq, ligne);
       }
   }
   //empty buffer after EOF
   refseqlist[cs].dna = malloc((strlen(tempseq)+1)*sizeof(char));
   if(refseqlist[cs].dna == NULL){
        fprintf(stderr,"Allocation not possible last refseqlist[cs].dna");
        exit(1);
    }
   strcpy(refseqlist[cs].dna, tempseq);

   refseqlist[cs].len = strlen(tempseq);
   if(refseqlist[cs].len > *plongerRefSeq){
        *plongerRefSeq = refseqlist[cs].len;
   }

    refseqlist[cs].rdna = malloc((strlen(tempseq)+1) * sizeof(char));
    if(refseqlist[cs].rdna == NULL){
         fprintf(stderr,"Allocation not possible refseqlist[cs].rdna");
         exit(1);
     }
    reverse_complement(refseqlist[cs]);

   cs++;
   return (cs);
}

int read_sequence(FILE *fp, struct Oneseq *myseq, int *plongerSeq, int *MaxSeqLength){
    // read one seq of a fasta file containing references sequences and store it in a Oneseq structure

    int  intTemp;
    int  nbSeqDone = 0;
    char tempseq[*MaxSeqLength];  //concatenation buffer
    char bufferligne[MAXLINE];
    fpos_t position;

    tempseq[0] = '\0'	; //buffer initialization

    //check struct Oneseq availability and free it
    if(myseq == NULL){
        fprintf(stderr, "Sorry, myseq struct pointer must be allocated!\n");
    }else{
        free(myseq->id);
        myseq->id = NULL;
        free(myseq->dna);
        myseq->dna = NULL;
        free(myseq->rdna);
        myseq->rdna = NULL;
    }

    //initialize position in stream
    fgetpos(fp, &position);

    while (fgets(bufferligne, MAXLINE, fp)){
        //--- check line length---
        if(strlen(bufferligne) >= (MAXLINE-1)){
            fprintf(stderr, "Length of line in the file (%d) exceed limit of %d\n", (int)strlen(bufferligne), MAXLINE);
            fprintf(stderr, "Line in file too long\n");
            exit(1);
        }
        my_chomp(bufferligne);

        //description line get id line
        if(bufferligne[0]=='>'){
            if(nbSeqDone > 0){
                fsetpos(fp, &position); //return to last line
                break;
            }
            myseq->id = malloc((strlen(bufferligne)+1)*sizeof(char));
            if(myseq->id == NULL){
                fprintf(stderr,"Allocation not possible myseq.id");
                exit(1);
            }
            strcpy(myseq->id, bufferligne);
            nbSeqDone++;
        }else{
            fgetpos(fp, &position);
            //read line of sequence
            intTemp = 0;
            intTemp += strlen(tempseq);
            intTemp += strlen(bufferligne); //note : direct sum of 2 strlen on the same line failed!!
            if(intTemp > *MaxSeqLength){
                fprintf(stderr, "Reference sequence too long");
                exit(1);
            }
            strcat(tempseq, bufferligne);
        }
    }

    //load sequence in OneSeq struct
    if(tempseq[0] != '\0'){     //in case of EOF while calling function
        myseq->dna = malloc((strlen(tempseq)+1) * sizeof(char));
        if(myseq->dna == NULL){
             fprintf(stderr,"Allocation not possible myseq.dna");
             exit(1);
         }
        strcpy(myseq->dna, tempseq);

        myseq->len = strlen(tempseq);
        if(myseq->len > *plongerSeq){
            *plongerSeq = myseq->len;
        }

        myseq->rdna = malloc((strlen(tempseq)+1) * sizeof(char));
        if(myseq->rdna == NULL){
             fprintf(stderr,"Allocation not possible myseq.rdna");
             exit(1);
         }
        reverse_complement(*myseq);
        return 1;
    }
    return 0;
}

void free_refseqlist(struct Oneseq *refseqlist, int nbRefSeq){
    int i=0;
    for (i = 0; i < nbRefSeq; i++) {
        free(refseqlist[i].dna);
        refseqlist[i].dna = NULL;
        free(refseqlist[i].rdna);
        refseqlist[i].rdna = NULL;
        free(refseqlist[i].id);
        refseqlist[i].id = NULL;
    }
    free(refseqlist);
    refseqlist = NULL;
}

//====================Stockage Resultat=======================


float **alloc_RESU_half_matrix(size_t nbSeqi){
        //allocate half of square matrix (left inf)
        size_t n=0;
        float **MRESU;
        MRESU = malloc(nbSeqi * sizeof (float *));
        if (MRESU == NULL) {
        fprintf(stderr, "Allocation impossible MRESU");
        exit(1);
    }
        for (size_t m = 0; m < nbSeqi; m++) {
                n++;
                MRESU[m] = malloc(n * sizeof (float));
                if (MRESU[m] == NULL) {
                        fprintf(stderr, "Allocation impossible MRESU[%zd]",m);
                        exit(1);
                }
                for (size_t i = 0; i < n; i++) {
                        MRESU[m][i] = 0.0; //sinon le test d'allocation memoire ne marche pas
                }
        }
        return MRESU;
}

void free_RESU_half_matrix(float **MRESU, size_t nbSeqi){
    for (size_t m = 0; m < nbSeqi; m++) {
        free(MRESU[m]);
    }
    free(MRESU);
}

float **alloc_RESU_matrix(size_t nbSeqi, size_t nbSeqj){
    float **MRESU;
    //std::vector <std::vector <float>> MRESU;
    MRESU = malloc(nbSeqi * sizeof (float *));
    //MRESU.resize(nbSeqi);
    if (MRESU == NULL) {
        fprintf(stderr, "Allocation impossible MRESU");
        exit(1);
    }
    for (size_t m = 0; m < nbSeqi; ++m) {
        MRESU[m] = malloc(nbSeqj * sizeof (float));
        //MRESU[m].resize(nbSeqj);
        if (MRESU[m] == NULL) {
                        fprintf(stderr, "Allocation impossible MRESU[%zd]",m);
                        exit(1);
                }
                for (size_t n = 0; n < nbSeqj; ++n) {
                        MRESU[m][n] = 0.0; //sinon le test d'allocation memoire ne marche pas
                }
    }

    return MRESU;
    //return NULL;
}

void free_RESU_matrix(float **MRESU, size_t nbSeqi, size_t nbSeqj){
    for (size_t m = 0; m < nbSeqi; ++m) {
        for (size_t j = 0; j < nbSeqj; j++) {
            MRESU[m][j] = 0;
        }
        free(MRESU[m]);
    }
    free(MRESU);
}

//=====================Espace calcul=====================

float **alloc_NW_matrix(int maxseqlen){
    int m=0;
    float **NW;
    NW = calloc(maxseqlen, sizeof (float *));
    for (m = 0; m < maxseqlen; ++m) {
        NW[m] = calloc(maxseqlen, sizeof (float));
    }
    if (NW == NULL) {
        fprintf(stderr, "Allocation impossible NW");
        exit(1);
    }
    return NW;
}

void free_NW_matrix(float **NW, int maxseqlen){
    int m=0, j=0;
    for (m = 0; m < maxseqlen; ++m) {
        for (j = 0; j < maxseqlen; j++) {
            NW[m][j] = 0;
        }
        free(NW[m]);
    }
    free(NW);
}

void print_NW_matrix(float **NW, int maxseqlen){
    int m=0, j=0;
    for (m = 0; m < maxseqlen; ++m) {
        for (j = 0; j < maxseqlen; j++) {
            printf("%.2f\t", NW[m][j]);
        }
        printf("\n");
    }
}

//====================Matrice de Score===================

float **build_comp_matrix(){
    int i=0, j=0;

    //allocation d'une table pour stockage ascii
    float **compMatrix;
    compMatrix = calloc(nb_ascii, sizeof (float *));
    for (i = 0; i < nb_ascii; ++i) {
        compMatrix[i] = calloc(nb_ascii, sizeof (float));
    }
    if (compMatrix == NULL) {
        fprintf(stderr, "Allocation impossible compMatrix");
        exit(1);
    }
    //remplissage matrice
    //--remplissage -1 par défaut
    for (i = 0; i < nb_ascii ; ++i) {
        for (j = 0; j < nb_ascii; j++) {
            compMatrix[i][j] = -1;
        }
    }
    //--remplissage diagonale 1 par défaut
    for (i = 0; i < nb_ascii ; ++i){
        compMatrix[i][i] = 1;
    }
    //--remplissage pour les "N" code ascii 78 et 110
    compMatrix[L_A][L_N] = (float)(compMatrix[L_A][L_A] + compMatrix[L_A][L_T] + compMatrix[L_A][L_G] + compMatrix[L_A][L_C]) / 4.0;
    compMatrix[L_N][L_A] = compMatrix[L_n][L_A] = compMatrix[L_N][L_a] = compMatrix[L_n][L_a] = compMatrix[L_a][L_N] = compMatrix[L_A][L_n] = compMatrix[L_a][L_n] = compMatrix[L_A][L_N];
    compMatrix[L_T][L_N] = (float)(compMatrix[L_T][L_A] + compMatrix[L_T][L_T] + compMatrix[L_T][L_G] + compMatrix[L_T][L_C]) / 4.0;
    compMatrix[L_N][L_T] = compMatrix[L_n][L_T] = compMatrix[L_N][L_t] = compMatrix[L_n][L_t] = compMatrix[L_t][L_N] = compMatrix[L_T][L_n] = compMatrix[L_t][L_n] = compMatrix[L_T][L_N];
    compMatrix[L_G][L_N] = (float)(compMatrix[L_G][L_A] + compMatrix[L_G][L_T] + compMatrix[L_G][L_G] + compMatrix[L_G][L_C]) / 4.0;
    compMatrix[L_N][L_G] = compMatrix[L_n][L_G] = compMatrix[L_N][L_g] = compMatrix[L_n][L_g] = compMatrix[L_g][L_N] = compMatrix[L_G][L_n] = compMatrix[L_g][L_n] = compMatrix[L_G][L_N];
    compMatrix[L_C][L_N] = (float)(compMatrix[L_C][L_A] + compMatrix[L_C][L_T] + compMatrix[L_C][L_G] + compMatrix[L_C][L_C]) / 4.0;
    compMatrix[L_N][L_C] = compMatrix[L_n][L_C] = compMatrix[L_N][L_c] = compMatrix[L_n][L_c] = compMatrix[L_c][L_N] = compMatrix[L_C][L_n] = compMatrix[L_c][L_n] = compMatrix[L_C][L_N];
    compMatrix[L_N][L_N] = compMatrix[L_N][L_n] = compMatrix[L_n][L_N] = compMatrix[L_n][L_n] = -0.5;

    //--remplissage des combinaisons minuscules / majuscule--
    compMatrix[L_A][L_a] = compMatrix[L_a][L_A] = compMatrix[L_T][L_t] = compMatrix[L_t][L_T] = compMatrix[L_G][L_g] =  compMatrix[L_g][L_G] =  compMatrix[L_C][L_c] = compMatrix[L_c][L_C] = 1;

    return compMatrix;
}

void free_comp_matrix(float **compMatrix){
    int m=0, j=0;
    for (m = 0; m < nb_ascii; ++m) {
        for (j = 0; j < nb_ascii; j++) {
            compMatrix[m][j] = 0;
        }
        free(compMatrix[m]);
    }
    free(compMatrix);
}

void print_comp_matrix(float **compMatrix){
    int i=0, j=0;
    int nucleotides[11] = {L_A, L_T, L_G, L_C, tiret, L_N, L_a, L_t, L_g, L_c, L_n};
    //impression matrice
    printf("comparison matrix: \n");
    for (i=0; i < nb_nucleotides; i++){
        printf("%c\t",nucleotides[i]);
        for (j=0; j< nb_nucleotides; j++){
            printf("%.2f\t",compMatrix[nucleotides[i]][nucleotides[j]]);
        }
        printf("\n");
    }
}

void print_comp_matrix_fileload(float **compMatrix){

    int i=0, j=0, k=0;
    int *items;
    items = calloc(nb_ascii, sizeof (int));

    //scan matrix to search recorded items
    for (i=0; i < nb_ascii; i++){
        for (j=0; j< nb_ascii; j++){
            if(compMatrix[i][j] != -666.0){
                items[k] = i;
                k++;
                break;
            }
        }
    }

    //impression matrice
    printf("matrice de comparaison : \n");
    //print header
    for (i=0; i < k; i++){
        printf("\t%c",items[i]);
    }
    printf("\n");
    for (i=0; i < k; i++){
        printf("%c\t",items[i]);
        for (j=0; j< k; j++){
            printf("%.2f\t",compMatrix[items[i]][items[j]]);
        }
        printf("\n");
    }
}

float **load_comp_matrix(FILE *fp){
    //read a tab separated txt file with comp matrix
    printf("loading comp matrix...\n");
    int i=0, j=0;
    char ligne[MAXLINE];

    //allocation d'une table pour stockage ascii
    float **compMatrix;
    compMatrix = calloc(nb_ascii, sizeof (float *));
    for (i = 0; i < nb_ascii; ++i) {
        compMatrix[i] = calloc(nb_ascii, sizeof (float));
    }
    if (compMatrix == NULL) {
        fprintf(stderr, "Allocation impossible compMatrix");
        exit(1);
    }
    //preremplissage table par defaut
    for (i = 0; i < nb_ascii ; ++i) {
        for (j = 0; j < nb_ascii; j++) {
            compMatrix[i][j] = -666.0;
        }
    }

    //lecture matrice

    //____recup ligne d'entete____
    if(fgets(ligne, MAXLINE, fp) == NULL){
                                        fprintf(stderr,"comp matrix file empty");
                                        exit(1);
                }
    my_chomp(ligne);
    const char d[2] = "\t";
    char *token;
    char *saveptr;
    char *columnaa;
    columnaa = malloc((nb_aa+1) * sizeof(char));
    if(columnaa == NULL){
         fprintf(stderr,"Allocation not possible columnaa");
         exit(1);
     }
    //____load columns ____
    /* get the first token */
    token = strtok_r(ligne, d, &saveptr);
    /* walk through other tokens */
    i=0;
    columnaa[i] = 'X';
    while( token != NULL ) {
        if((int)strlen(token) > 1 && i!=0){
            fprintf(stderr, "Pb with comparaison matrix file, code %s exceed 1 character\n",token);
            exit(1);
        }
        if(i != 0){
            columnaa[i] = token[0];
        }
        i++;
        token = strtok_r(NULL, d, &saveptr);
    }

    //____load values_______
    for(i=0; fgets(ligne, MAXLINE, fp); ++i){
        my_chomp(ligne);
        char *current_aa;
        char *saveptr2;
        int j;
        float currentval;
        char *fin = NULL;
        /* get the current aa */
        current_aa = strtok_r(ligne, d, &saveptr2);
        if((int)strlen(current_aa) > 1){
            fprintf(stderr, "Pb with comparaison matrix file, code %s exceed 1 character\n",current_aa);
            exit(1);
        }

        /* walk through other tokens */
        //lecture premiere valeur
        j=1;
        currentval = (float)strtod(saveptr2, &fin);
        compMatrix[(int)current_aa[0]][(int)columnaa[j]] = currentval;
        //lecture suite
        while(fin[0] != '\0'){
            j++;
            currentval = (float)strtod(++fin, &fin);
            compMatrix[(int)current_aa[0]][(int)columnaa[j]] = currentval;
        }
    }
    //--remplissage des combinaisons minuscules / majuscule--

    return compMatrix;
}

int invalid_char(char *my_char, float **compMatrix){
    //test if current char is valid : i.e. defined in comp_matrix
    //return 1 if char is not valid
    int cur_char = *my_char;
    if(cur_char <= nb_ascii){
        if(compMatrix[cur_char][cur_char] != -666.0){
            return 0;
        }
    }
    return 1;
}

int invalid_string(char *my_string, float **compMatrix){
    //test if current string is valid : i.e. defined in comp_matrix
    //return 0 if string is valid or ascii number if string is invalid
    int i;
    int cur_char;
    float def_val;
    def_val = -666.0;
    for (i = 0; i < strlen(my_string); ++i) {
        cur_char = my_string[i];
        if(cur_char > nb_ascii){
            return cur_char;
        }
        if(compMatrix[cur_char][cur_char] == def_val){
            return cur_char;
        }
    }
    return 0;
}

void fill_penality_comp_matrix(float **compMatrix){
    //rempli la matrice de comparaison avec une matrice unitaire de score / gain
    int i=0, j=0;

    //remplissage matrice
    //--remplissage 1 par défaut
    for (i = 0; i < nb_ascii ; ++i) {
        for (j = 0; j < nb_ascii; j++) {
            compMatrix[i][j] = 1;
        }
    }
    //--remplissage diagonale 0 par défaut
    for (i = 0; i < nb_ascii ; ++i){
        compMatrix[i][i] = 0;
    }
    //--remplissage pour les "N" code ascii 78 et 110

    //--remplissage des combinaisons minuscules / majuscule--
    compMatrix[L_A][L_a] = compMatrix[L_a][L_A] = compMatrix[L_T][L_t] = compMatrix[L_t][L_T] = compMatrix[L_G][L_g] =  compMatrix[L_g][L_G] =  compMatrix[L_C][L_c] = compMatrix[L_c][L_C] = 0;

}

//=======================ALGORITHMES======================
int matchexact(struct Oneseq struC, struct Oneseq struL){
        //      search if sub-string struC is included in sub-string struL
        //      return start position of match in struL string or -1
    char *ptr_struL = NULL;
    char *ptr_struL_rev = NULL;
    int index = 0;
    ptr_struL = strstr(struL.dna,struC.dna);
    if(struC.rdna != NULL){ptr_struL_rev = strstr(struL.dna,struC.rdna);}
    else if(struL.rdna != NULL){ptr_struL_rev = strstr(struL.rdna,struC.dna);}
    if((ptr_struL == NULL) && (ptr_struL_rev == NULL)){
        return -1;
    }else{
        if(ptr_struL != NULL){
                index = (ptr_struL - struL.dna) + 1;
        }else{
                index = (ptr_struL_rev - struL.dna) + 1;
        }
        return index;
    }
}

float nw_distance(struct Oneseq stru1, struct Oneseq stru2, float **NW) {
    //      (char *seq1, char *seq2, int n1, int n2)
    //          crée la matrice de l'algorithme de Needleman-Wunsh
    //	Lit le dernier element en bas a droite
    //	Le retourne

    int i, j = 0;
    float tt = 0.0;
    float x1, x2, x3 = 0.0;
    float res, res1, res2 = 0.0;

    for (i = 0; i < stru1.len + 1; ++i) {
        NW[i][0] = i;
    }
    for (j = 0; j < stru2.len + 1; ++j) {
        NW[0][j] = j;
    }

    //paire dans le sens initial
    for (i = 1; i < stru1.len + 1; ++i) {
        for (j = 1; j < stru2.len + 1; ++j) {
            tt = 1;
            x1 = NW[i - 1][j]; // d(i-1,j)
            x2 = NW[i][j - 1]; // d(i,j-1)
            x3 = NW[i - 1][j - 1]; // d(i-1,j-1)

            if (stru1.dna[i - 1] == stru2.dna[j - 1]) {
                tt = 0;
            }
            NW[i][j] = MIN3(1 + x1, 1 + x2, tt + x3); // d(i,j)
        }
    }
    res1 = NW[stru1.len][stru2.len];
    //paire avec inverse complémentaire sur stru2
    for (i = 1; i < stru1.len + 1; ++i) {
        for (j = 1; j < stru2.len + 1; ++j) {
            tt = 1;
            x1 = NW[i - 1][j]; // d(i-1,j)
            x2 = NW[i][j - 1]; // d(i,j-1)
            x3 = NW[i - 1][j - 1]; // d(i-1,j-1)

            if (stru1.dna[i - 1] == stru2.rdna[j - 1]) {
                tt = 0;
            }
            NW[i][j] = MIN3(1 + x1, 1 + x2, tt + x3); // d(i,j)
        }
    }
    res2 = NW[stru1.len][stru2.len];
    res = MIN2(res1, res2);
    return res;
}

float nw_traceback(struct Oneseq stru1, struct Oneseq stru2, float **NW, float **TBK, float **TBKrev, float **comp_matrix, char *stru1_align, char *stru2_align) {
    //algo de needleman avec calcul d'homology et traceback
    int i, j = 0;
    float **TBKchoix = NULL;
    char *stru2seq = NULL;      //pointeur sur seq sens ou reverse complémentaire
    float min_s, res, res1, res2 = 0.0;
    float d_score, h_score, v_score = 0;
    int tiret = '-';
    int nuc_1, nuc_2 = 0;       //indice des nucleotides selon table comparaison
    //int maxalign = stru1.len + stru2.len;       //longueur max de l'alignement
    //int lengthalign = 0;        //longueur alignement

    //initialisation ligne et colonne
    for (i = 0; i <= stru1.len; ++i) {
        NW[i][0] = i;
    }
    for (j = 0; j <= stru2.len; ++j) {
        NW[0][j] = j;
    }
    //remplissage matrice sens initial
    for (i = 1; i <= stru1.len; ++i) {
        for (j = 1; j <= stru2.len; ++j) {
            nuc_1 = stru1.dna[i - 1];
            nuc_2 = stru2.dna[j - 1];
            v_score = (NW[i - 1][j]) + (comp_matrix[nuc_1][tiret]); // d(i-1,j)
            h_score = (NW[i][j - 1]) + (comp_matrix[tiret][nuc_2]); // d(i,j-1)
            d_score = (NW[i - 1][j - 1]) + (comp_matrix[nuc_1][nuc_2]); // d(i-1,j-1)
            min_s = MIN3(v_score, h_score, d_score);
            NW[i][j] = min_s;
            //alimentation matrice de traceback
            if(d_score == min_s){       //priorité diagonale
                TBK[i][j] = 2;
            }else if(v_score == min_s){
                TBK[i][j] = 3;
            }else if(h_score == min_s){
                TBK[i][j] = 1;
            }
        }
    }
    res1 = NW[stru1.len][stru2.len];

    //remplissage matrice sens reverse complémentaire stru2
    for (i = 1; i <= stru1.len; ++i) {
        for (j = 1; j <= stru2.len; ++j) {
            nuc_1 = stru1.dna[i - 1];
            nuc_2 = stru2.rdna[j - 1];
            v_score = (NW[i - 1][j]) + (comp_matrix[nuc_1][tiret]); // d(i-1,j)
            h_score = (NW[i][j - 1]) + (comp_matrix[tiret][nuc_2]); // d(i,j-1)
            d_score = (NW[i - 1][j - 1]) + (comp_matrix[nuc_1][nuc_2]); // d(i-1,j-1)
            min_s = MIN3(v_score, h_score, d_score);
            NW[i][j] = min_s;
            //alimentation matrice de traceback
            if(d_score == min_s){       //priorité diagonale
                TBKrev[i][j] = 2;
            }else if(v_score == min_s){
                TBKrev[i][j] = 3;
            }else if(h_score == min_s){
                TBKrev[i][j] = 1;
            }
        }
    }
    res2 = NW[stru1.len][stru2.len];

    //choix sens optimal
    if(res1 < res2){
        res = res1;
        TBKchoix = TBK;
        stru2seq = stru2.dna;
    }else{
        res = res2;
        TBKchoix = TBKrev;
        stru2seq = stru2.rdna;
    }

    //traceback
    i = stru1.len;
    j = stru2.len;
    int ii = 0;
    int jj = 0;
    stru1_align[ii] = '\0';
    stru2_align[jj] = '\0';
    while ((i > 0) && (j > 0)){
        if(TBKchoix[i][j] == 2){     //diagonale
            stru1_align[ii] = stru1.dna[i - 1];
            stru2_align[jj] = stru2seq[j - 1];
            i--;
            j--;
        }else if(TBKchoix[i][j] == 3){       //verticale
            stru1_align[ii] = stru1.dna[i - 1];
            stru2_align[jj] = '-';
            i--;
        }else{  //horizontale
            stru1_align[ii] = '-';
            stru2_align[jj] = stru2seq[j - 1];
            j--;
        }
        ii++;
        jj++;
    }
    stru1_align[ii] = '\0';
    stru2_align[jj] = '\0';

    //impression matrice
/*
    for (i = 0; i <= stru1.len; ++i) {
        for (j = 0; j <= stru2.len; ++j) {
            printf("%.2f\t",TBK[i][j]);
        }
        printf("\n");
    }
*/
    //impression alignement
    printf("%s vs %s\n",stru1.id,stru2.id);
    for(i = (strlen(stru1_align) - 1); i >= 0; i--){
        printf("%c",stru1_align[i]);
    }
    printf("\n");
    for(j = (strlen(stru2_align) - 1); j >= 0; j--){
        printf("%c",stru2_align[j]);
    }
    printf("\n");

    return res;
}

/*___SW element__*/
int init_SW_matrix(float **SW, int maxseqlen){
    //initialise première ligne et col de la matrice
    int i, j = 0;
    //--initialisation premiere ligne et col
    for (i = 0; i < maxseqlen + 1; ++i) {
        SW[i][0] = 0 ;
    }
    for (j = 0; j < maxseqlen + 1; ++j) {
        SW[0][j] = 0 ;
    }
    return 1;
}

float max_score_seq(char *string, float **comp_matrix) {
    //compute max score from a sequence
    int i = 0;
    float res = 0.0;
    int cur_char;
    for (i = 0; i < strlen(string); ++i) {
        cur_char = string[i];
        res += comp_matrix[cur_char][cur_char];
    }
    return res;
}

float sw_compute(char *string1, char *string2, float **SW, float **comp_matrix) {
    //      (char *seq1, char *seq2, int n1, int n2)
    //          rempli la matrice SW de l'algorithme de Smith-waterman
    //	recherche l'element le plus élevé
    //	Le retourne

    //!!!N'effectue que le calcul sans effectuer d'inverse complementaire!!!
    //element de calcul utilisé pour d'autres fonctions
    // pour une fonction de renvoi de distance => voir sw_distance
    //suppose que première ligne et col soient déjà initialisées

    int i, j = 0;
    int char_1, char_2 = 0;       //indice des nucleotides selon table comparaison
    float x1, x2, x3 = 0;
    float res = 0.0;
    float max_res = 0.0;
    float dis = 0.0;
    int tiret = '-';

    //--remplissage matrice
    for (i = 1; i < strlen(string1) + 1; ++i) {
        for (j = 1; j < strlen(string2) + 1; ++j) {
            char_1 = string1[i - 1];
            char_2 = string2[j - 1];
            x1 = SW[i - 1][j] + comp_matrix[char_1][tiret]; // d(i-1,j)
            x2 = SW[i][j - 1] + comp_matrix[tiret][char_2]; // d(i,j-1)
            x3 = SW[i - 1][j - 1] + comp_matrix[char_1][char_2]; // d(i-1,j-1)
            SW[i][j] = MAX3(x1, x2, x3);
            if(SW[i][j] < 0.0){SW[i][j] = 0.0;}
            if(SW[i][j] > res){res = SW[i][j];}
        }
    }

    //compute max score from smallest seq
    if(strlen(string1) < strlen(string2)){
        max_res = max_score_seq(string1, comp_matrix);
    }else{
        max_res = max_score_seq(string2, comp_matrix);
    }

    //compute distance
    dis = max_res - res;

    return dis;
}

float sw_distance_txtx(struct Oneseq stru1, struct Oneseq stru2, float **SW, float **comp_matrix){
    //perform sw on translated stru1 AND translated stru2
    //get sequences in uppercase
    char *stru1_seq;
    char *stru1_rseq;
    char *stru2_seq;
    char *stru2_rseq;
    stru1_seq = malloc((strlen(stru1.dna)+1) * sizeof(char));
    stru1_rseq = malloc((strlen(stru1.rdna)+1) * sizeof(char));
    stru2_seq = malloc((strlen(stru2.dna)+1) * sizeof(char));
    stru2_rseq = malloc((strlen(stru2.rdna)+1) * sizeof(char));

    if((stru1_seq == NULL)||(stru1_rseq == NULL)||(stru2_seq == NULL)||(stru2_rseq == NULL)){
         fprintf(stderr,"Allocation not possible strux_seq");
         exit(1);
    }
    strcpy(stru1_seq, stru1.dna);
    strcpy(stru1_rseq, stru1.rdna);
    strcpy(stru2_seq, stru2.dna);
    strcpy(stru2_rseq, stru2.rdna);
    convertToUpperCase(stru1_seq);
    convertToUpperCase(stru1_rseq);
    convertToUpperCase(stru2_seq);
    convertToUpperCase(stru2_rseq);

    //allocation listes de string
    char **liste1_pstring;
    liste1_pstring = malloc(6 * sizeof (char*));
    if (liste1_pstring == NULL) {
        fprintf(stderr, "Allocation impossible liste1_pstring");
        exit(1);
    }
    char **liste2_pstring;
    liste2_pstring = malloc(6 * sizeof (char*));
    if (liste2_pstring == NULL) {
        fprintf(stderr, "Allocation impossible liste2_pstring");
        exit(1);
    }
    //traduction string1
    int i, j = 0;
    for(i=0; i<3; i++){
        liste1_pstring[j] = translate_dnaseq(stru1_seq + i);
        j++;
    }
    for(i=0; i<3; i++){
        liste1_pstring[j] = translate_dnaseq(stru1_rseq + i);
        j++;
    }
    //test string validation
    int teststring;
    for(i=0; i<j; i++){
        teststring = invalid_string(liste1_pstring[i], comp_matrix);
        if(teststring != 0){
            fprintf(stderr,"Translated String1 contain character %c not supplied in comparison matrix\n", teststring);
            exit(1);
        }
    }

    //traduction string2
    i = 0;
    j = 0;
    for(i=0; i<3; i++){
        liste2_pstring[j] = translate_dnaseq(stru2_seq + i);
        j++;
    }
    for(i=0; i<3; i++){
        liste2_pstring[j] = translate_dnaseq(stru2_rseq + i);
        j++;
    }
    //test string validation
    for(i=0; i<j; i++){
        teststring = invalid_string(liste2_pstring[i], comp_matrix);
        if(teststring != 0){
            fprintf(stderr,"Translated String2 contain character %c not supplied in comparison matrix\n", teststring);
            exit(1);
        }
    }

    //calcul distances
    float dis, dis1=0.0;
    dis = -666.0;
    for (i=0; i<6; i++){
        for(j=0; j<6; j++){
            dis1 = sw_compute(liste1_pstring[i], liste2_pstring[j], SW, comp_matrix);
            if((i==0)&&(j==0)){
                dis = dis1;     //initialisation dis
            }else{
                dis = MIN2(dis,dis1);
            }
        }
    }
    //free space
    free(stru1_seq);
    free(stru1_rseq);
    free(stru2_seq);
    free(stru2_rseq);
    for(i=0; i<6; i++){
        free(liste1_pstring[i]);
        liste1_pstring[i]=NULL;
    }
    free(liste1_pstring);
    for(i=0; i<6; i++){
        free(liste2_pstring[i]);
        liste2_pstring[i]=NULL;
    }
    free(liste2_pstring);

    return dis;
}

/*___SW distance___*/
float sw_distance(struct Oneseq stru1, struct Oneseq stru2, float **SW, float **comp_matrix) {
    //      (char *seq1, char *seq2, int n1, int n2)
    //          rempli la matrice SW de l'algorithme de Smith-waterman
    //	recherche l'element le plus élevé
    //	Le retourne
    int i, j = 0;
    int nuc_1, nuc_2 = 0;       //indice des nucleotides selon table comparaison
    float x1, x2, x3 = 0;
    float res = 0.0;
    float dis, dis1, dis2 = 0.0;
    int tiret = '-';

    //--initialisation premiere ligne et col
    for (i = 0; i < stru1.len + 1; ++i) {
        SW[i][0] = 0 ;
    }
    for (j = 0; j < stru2.len + 1; ++j) {
        SW[0][j] = 0 ;
    }

    //--remplissage matrice sens initial
    for (i = 1; i < stru1.len + 1; ++i) {
        for (j = 1; j < stru2.len + 1; ++j) {
            nuc_1 = stru1.dna[i - 1];
            nuc_2 = stru2.dna[j - 1];
            x1 = SW[i - 1][j] + comp_matrix[nuc_1][tiret]; // d(i-1,j)
            x2 = SW[i][j - 1] + comp_matrix[tiret][nuc_2]; // d(i,j-1)
            x3 = SW[i - 1][j - 1] + comp_matrix[nuc_1][nuc_2]; // d(i-1,j-1)
            SW[i][j] = MAX3(x1, x2, x3);
            if(SW[i][j] < 0.0){SW[i][j] = 0.0;}
            if(SW[i][j] > res){res = SW[i][j];}
        }
    }
    //fprintf(stderr,"___%f___\t",res);
    dis1 = ((MIN2(stru1.len, stru2.len) - res) / 2.0) ;
    //fprintf(stderr,"___%f___\n",dis1);

    //--remplissage matrice sens inverse complementaire stru2
    res = 0.0;
    for (i = 1; i < stru1.len + 1; ++i) {
        for (j = 1; j < stru2.len + 1; ++j) {
            nuc_1 = stru1.dna[i - 1];
            nuc_2 = stru2.rdna[j - 1];
            x1 = SW[i - 1][j] + comp_matrix[nuc_1][tiret]; // d(i-1,j)
            x2 = SW[i][j - 1] + comp_matrix[tiret][nuc_2]; // d(i,j-1)
            x3 = SW[i - 1][j - 1] + comp_matrix[nuc_1][nuc_2]; // d(i-1,j-1)
            SW[i][j] = MAX3(x1, x2, x3);
            if(SW[i][j] < 0.0){SW[i][j] = 0.0;}
            if(SW[i][j] > res){res = SW[i][j];}
        }
    }
    //fprintf(stderr,"___%f___\t",res);
    dis2 = ((MIN2(stru1.len, stru2.len) - res) / 2.0) ;
    //fprintf(stderr,"___%f___\n",dis2);
    dis = MIN2(dis1,dis2);
    //fprintf(stderr,"___%d___%d\n",stru1.len,stru2.len);
    return dis;
}

float sw_traceback(struct Oneseq stru1, struct Oneseq stru2, float **SW, float **SWrev, float **TBK, float **TBKrev, float **comp_matrix, char *stru1_align, char *stru2_align) {
    //algo de smith-waterman et calcul du trace back
    int i=0, j=0;
    int i_best=0, j_best=0;
    int i_best1=0, j_best1=0, i_best2=0, j_best2=0;
    int i_start=0, j_start=0;
    float **SWchoix = NULL;
    float **TBKchoix = NULL;
    char *stru2seq = NULL;      //pointeur sur seq sens ou reverse complémentaire
    int nuc_1, nuc_2 = 0;       //indice des nucleotides selon table comparaison
    float d_score, h_score, v_score = 0;
    float max_s, res, res1, res2;
    float dis = 0.0;
    int tiret = '-';

    //--initialisation premiere ligne et col
    for (i = 0; i < stru1.len + 1; ++i) {
        SW[i][0] = 0 ;
        SWrev[i][0] = 0 ;
    }
    for (j = 0; j < stru2.len + 1; ++j) {
        SW[0][j] = 0 ;
        SWrev[i][0] = 0 ;
    }

    //--remplissage matrice sens initial
    res1 = 0.0;
    for (i = 1; i < stru1.len + 1; ++i) {
        for (j = 1; j < stru2.len + 1; ++j) {
            nuc_1 = stru1.dna[i - 1];
            nuc_2 = stru2.dna[j - 1];
            v_score = SW[i - 1][j] + comp_matrix[nuc_1][tiret]; // d(i-1,j)
            h_score = SW[i][j - 1] + comp_matrix[tiret][nuc_2]; // d(i,j-1)
            d_score = SW[i - 1][j - 1] + comp_matrix[nuc_1][nuc_2]; // d(i-1,j-1)
            max_s = MAX3(v_score, h_score, d_score);
            if(max_s <= 0.0){
                SW[i][j] = 0.0;
                TBK[i][j] = 0.0;
            }else{
                SW[i][j] = max_s;
                //alimentation matrice de traceback
                if(d_score == max_s){       //priorité diagonale
                    TBK[i][j] = 2;
                }else if(v_score == max_s){
                    TBK[i][j] = 3;
                }else if(h_score == max_s){
                    TBK[i][j] = 1;
                }
            }
            if(max_s > res1){    //stockage score le plus élevé de la matrice
                res1 = SW[i][j];
                i_best1 = i;
                j_best1 = j;
            }
        }
    }

    //--remplissage matrice sens reverse complementaire sur stru2
    res2 = 0.0;
    for (i = 1; i < stru1.len + 1; ++i) {
        for (j = 1; j < stru2.len + 1; ++j) {
            nuc_1 = stru1.dna[i - 1];
            nuc_2 = stru2.rdna[j - 1];
            v_score = SWrev[i - 1][j] + comp_matrix[nuc_1][tiret]; // d(i-1,j)
            h_score = SWrev[i][j - 1] + comp_matrix[tiret][nuc_2]; // d(i,j-1)
            d_score = SWrev[i - 1][j - 1] + comp_matrix[nuc_1][nuc_2]; // d(i-1,j-1)
            max_s = MAX3(v_score, h_score, d_score);
            if(max_s <= 0.0){
                SWrev[i][j] = 0.0;
                TBKrev[i][j] = 0.0;
            }else{
                SWrev[i][j] = max_s;
                //alimentation matrice de traceback
                if(d_score == max_s){       //priorité diagonale
                    TBKrev[i][j] = 2;
                }else if(v_score == max_s){
                    TBKrev[i][j] = 3;
                }else if(h_score == max_s){
                    TBKrev[i][j] = 1;
                }
            }
            if(max_s > res2){    //stockage score le plus élevé de la matrice
                res2 = SWrev[i][j];
                i_best2 = i;
                j_best2 = j;
            }
        }
    }

    //choix optimal
    res = 0.0;
    if(res1 > res2){
        i_best = i_best1;
        j_best = j_best1;
        res = res1;
        SWchoix = SW;
        TBKchoix = TBK;
        stru2seq = stru2.dna;
    }else{
        i_best = i_best2;
        j_best = j_best2;
        res = res2;
        SWchoix = SWrev;
        TBKchoix = TBKrev;
        stru2seq = stru2.rdna;
    }

    //traceback
    i = i_best;
    j = j_best;
    i_start = i_best;
    j_start = j_best;
    int stru1indel = 0;
    int stru2indel = 0;
    int struSNP = 0;
    int ii=0;
    int jj =0;
    stru1_align[ii] = '\0';
    stru2_align[jj] = '\0';
    while((SWchoix[i][j] > 0) && (i > 0) & (j > 0)){
        if(TBKchoix[i][j] == 2){     //diagonale
            stru1_align[ii] = stru1.dna[i - 1];
            stru2_align[jj] = stru2seq[j - 1];
            if(stru1.dna[i - 1] != stru2seq[j - 1]){
                struSNP++;
            }
            i--;
            j--;
        }else if(TBKchoix[i][j] == 3){       //verticale
            stru1_align[ii] = stru1.dna[i - 1];
            stru2_align[jj] = '-';
            i--;
            stru2indel++;
        }else{  //horizontale
            stru1_align[ii] = '-';
            stru2_align[jj] = stru2seq[j - 1];
            j--;
            stru1indel++;
        }
        ii++;
        jj++;
        i_start = i;
        j_start = j;
    }
    stru1_align[ii] = '\0';
    stru2_align[jj] = '\0';

    dis = ((MIN2(stru1.len, stru2.len) - res) / 2.0) ;

    //=====impression alignement=====
    int k=0;

    printf("%s : %d vs %s : %d\n",stru1.id,stru1.len,stru2.id,stru2.len);
    printf("score:%.2f\tdistance:%.2f\n",res,dis);
    printf("i_best:%d\tstru1.len:%d\talignlen:%zu\tstartalign1:%d\n",i_best,stru1.len,strlen(stru1_align),i_start);
    printf("j_best:%d\tstru2.len:%d\talignlen:%zu\tstartalign2:%d\n",j_best,stru2.len,strlen(stru2_align),j_start);
    int stru1endout = ((stru1.len - i_best) - (stru2.len - j_best));
    if(stru1endout > 0 ){printf("stru1 end > stru2 end : %d\n", stru1endout);}
    int stru1startout = (i_start - j_start);
    if(stru1startout > 0 ){printf("stru1 start > stru2 start : %d\n", stru1startout);}
    int stru2endout = ((stru2.len - j_best) - (stru1.len - i_best));
    if(stru2endout > 0 ){printf("stru2 end > stru1 end : %d\n", stru2endout);}
    int stru2startout = (j_start - i_start);
    if(stru2startout > 0 ){printf("stru2 start > stru1 start : %d\n", stru2startout);}
    printf("nb insertion : stru1=%d, stru2=%d, nb SNP : %d\n",stru1indel,stru2indel,struSNP);

    //traitement region bordante debut
    for (k = 0; k < (j_start - i_start); k++){
        printf(".");
    }
    for (i = 0; i < i_start; i++){
        printf("%c",stru1.dna[i]);
    }
    printf("||");
    //traitement region alignée
    for(i = (ii - 1); i >= 0; i--){
        printf("%c",stru1_align[i]);
    }
    printf("||");
    //traitement region bordante fin
    for(i = i_best; i < stru1.len; i++){
        printf("%c",stru1.dna[i]);
    }
    for (k = 0; k < ((stru2.len - j_best) - (stru1.len - i_best)); k++){
        printf(".");
    }
    printf("\n");
    //____stru2____
    //traitement region bordante debut
    for (k = 0; k < (i_start - j_start); k++){
        printf(".");
    }
    for(j = 0; j < j_start; j++){
        printf("%c",stru2seq[j]);
    }
    printf("||");
    //traitement region alignée
    for(j = (jj - 1); j >= 0; j--){
        printf("%c",stru2_align[j]);
    }
    printf("||");
    //traitement region bordante fin
    for(j = j_best; j < stru2.len; j++){
        printf("%c",stru2seq[j]);
    }
    for (k = 0; k < ((stru1.len - i_best) - (stru2.len - j_best)); k++){
        printf(".");
    }
    printf("\n");

    return dis;
}

float dis_L1(float *histo1, float *histo2, unsigned long nb_item){
        //compute distance between 2 float array of nb_item
    float delta;
    float resultat;
    delta = 0.0;
    resultat = 0.0;
    int i;
    for (i = 0; i < nb_item; i++){
        delta = (float)fabs(histo1[i] - histo2[i]);
        resultat = resultat + delta;
    }
    return resultat;
}

//===================Progression & Sortie Résultat====================

void writeEDmat(float **MRESU, int maxseqi, int maxseqj, char outfile[], int *prIds, int *prLength, struct Oneseq *seqlisti, struct Oneseq *seqlistj) {
    /*soit la matrice MRESU au format MRESU[i][j] */
    /*print un tableau tabulé avec les i en lignes et les j en colonnes*/
    FILE *fresu;
    if ((fresu = fopen(outfile, "a")) == NULL) {
        printf("%s !!! Cannot open file !!! \n\n", outfile);
        exit(1);
    }

    FILE *handleBinFile = NULL ;

    int i, j;
    //ligne entete
    fprintf(fresu,"SEQ_ID");
    if (*prIds == 1) {
        for (j = 0; j <= maxseqj; ++j) {
            fprintf(fresu, "\t%s", seqlistj[j].id + 1);      //le +1 permet de décaler le pointeur pour éliminer le ">" de début
            if(*prLength ==1){fprintf(fresu,"|%d",seqlistj[j].len);}
        }
        fprintf(fresu, "\n");
    }

    for (i = 0; i <= maxseqi; ++i) {
        //id de sequence en debut de la ligne
        if (*prIds == 1) {
                fprintf(fresu, "%s", seqlisti[i].id + 1);      //le +1 permet de décaler le pointeur pour éliminer le ">" de début
                if(*prLength ==1){fprintf(fresu,"|%d",seqlisti[i].len);}
                fprintf(fresu, "\t");
        }
        for (j = 0; j <= maxseqj; ++j) {
            fprintf(fresu, "%.2f", MRESU[i][j]);
            if (j != maxseqj) {
                fprintf(fresu, "\t");
            }
        }
        fprintf(fresu, "\n");
    }
    fclose(fresu);
}

void writeHdf5(int nrows, int ncols, int nrowsmpi, int offsetmpi, float *distances, char **seqidqueryh5, char **seqidrefh5, char **wordqueryh5, char **wordrefh5, char outfile[]) {

    int mpirank = 0;
    int mpinp = 1;
    int mpiinit = 0;
    hid_t file_id;
    hid_t plist_id = H5P_DEFAULT;
#if defined(MPI)
    MPI_Initialized(&mpiinit);
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;
    if ( mpiinit ){
        MPI_Comm_rank(comm, &mpirank);
        MPI_Comm_size(comm, &mpinp);
        /*
         * Set up file access property list with parallel I/O access
         */
        plist_id = H5Pcreate(H5P_FILE_ACCESS);
        H5Pset_fapl_mpio(plist_id, comm, info);
    }
#endif

    /* create a new file collectively */
    file_id = H5Fcreate(outfile, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    herr_t status = H5Pclose(plist_id);

    /* Save the distance matrix */

    /* Dataset must be chunked for compression */
    hsize_t cdims[2] = {1, ncols};
    plist_id = H5Pcreate(H5P_DATASET_CREATE);
    status = H5Pset_chunk(plist_id, 2, cdims);

    /* Set ZLIB / DEFLATE Compression using compression level 5. */
    status = H5Pset_deflate(plist_id, 5);

    /* Create the data space for the dataset. */
    hsize_t dims[2] = {nrows, ncols};
    hid_t dataspace_id = H5Screate_simple(2, dims, NULL);

    /* Create the dataset. */
    hid_t dataset_id = H5Dcreate2(file_id, "/distances", H5T_IEEE_F32LE,
                                  dataspace_id, H5P_DEFAULT, plist_id, H5P_DEFAULT);
    /* Terminate access to properties */
    status = H5Pclose(plist_id);
    /* Terminate access to the data space. */
    status = H5Sclose(dataspace_id);

    hsize_t offset[2], stride[2], count[2], block[2];
    offset[0] = (hsize_t)offsetmpi;
    offset[1] = 0;
    stride[0] = 1;
    stride[1] = 1;
    count[0] = 1;
    count[1] = 1;
    block[0] = (hsize_t)nrowsmpi;
    block[1] = (hsize_t)ncols;
    /* create memory space */
    hid_t memspace_id  = H5Screate_simple(2, block, NULL);
    /* select hyperslab in the file */
    dataspace_id = H5Dget_space(dataset_id);
    H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, stride, count, block);
#if defined(MPI)
    if ( mpiinit ){
        /*
         * Create property list for collective dataset write.
         */
        plist_id = H5Pcreate(H5P_DATASET_XFER);
        H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
    } else {
        plist_id = H5P_DEFAULT;
    }
#else
    plist_id = H5P_DEFAULT;
#endif

    /* Write the dataset. */
    status = H5Dwrite(dataset_id, H5T_IEEE_F32LE, memspace_id, dataspace_id, plist_id, distances);

    /* End access to the dataset and release resources used by it. */
    status = H5Dclose(dataset_id);
    status = H5Sclose(memspace_id);
    status = H5Sclose(dataspace_id);
    status = H5Pclose(plist_id);
    /* Close the file. */
    status = H5Fclose(file_id);

    if (mpirank==0) {

        /* Save the Sequence ids */
        file_id = H5Fopen(outfile, H5F_ACC_RDWR, H5P_DEFAULT);

        /* Create file and memory datatypes */
        hid_t filetype_id = H5Tcopy (H5T_C_S1);
        status = H5Tset_size (filetype_id, H5T_VARIABLE);
        hid_t memtype_id = H5Tcopy (H5T_C_S1);
        status = H5Tset_size (memtype_id, H5T_VARIABLE);
        
        /* refFile == queryFile */
        if (seqidrefh5 == NULL){

          /* first : seqidqueryh5 */

          /* if dataset already exist delete it */
          htri_t datasetexist = -1;
          H5E_BEGIN_TRY
          datasetexist = H5Lexists(file_id, "seqid", H5P_DEFAULT);
          H5E_END_TRY
          if (datasetexist > 0) {
              H5Ldelete(file_id, "seqid", H5P_DEFAULT);
          }

          /* Create dataspace.  Setting maximum size to NULL sets the maximum size to
          be the current size */
          hsize_t sdims[1] = {(hsize_t)nrows};
          dataspace_id = H5Screate_simple (1, sdims, NULL);
          /* Create the dataset and write the variable-length string data to it */
          dataset_id = H5Dcreate (file_id, "seqid", filetype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
          /* save the char* array */
          H5Dwrite (dataset_id, memtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, seqidqueryh5);
          status = H5Dclose (dataset_id);
          status = H5Sclose (dataspace_id);
          
          /* third : wordqueryh5 */

          /* if dataset already exist delete it */
          datasetexist = -1;
          H5E_BEGIN_TRY
          datasetexist = H5Lexists(file_id, "word", H5P_DEFAULT);
          H5E_END_TRY
          if (datasetexist > 0) {
              H5Ldelete(file_id, "word", H5P_DEFAULT);
          }

          /* Create dataspace.  Setting maximum size to NULL sets the maximum size to
          be the current size */
          sdims[0] = (hsize_t)nrows;
          dataspace_id = H5Screate_simple (1, sdims, NULL);
          /* Create the dataset and write the variable-length string data to it */
          dataset_id = H5Dcreate (file_id, "word", filetype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
          /* save the char* array */
          H5Dwrite (dataset_id, memtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, wordqueryh5);
          status = H5Dclose (dataset_id);
          status = H5Sclose (dataspace_id);
        }

        /* refFile == queryFile */
        else
        {
          /* first : seqidqueryh5 */

          /* if dataset already exist delete it */
          htri_t datasetexist = -1;
          H5E_BEGIN_TRY
          datasetexist = H5Lexists(file_id, "row_seqid", H5P_DEFAULT);
          H5E_END_TRY
          if (datasetexist > 0) {
              H5Ldelete(file_id, "row_seqid", H5P_DEFAULT);
          }

          /* Create dataspace.  Setting maximum size to NULL sets the maximum size to
          be the current size */
          hsize_t sdims[1] = {(hsize_t)nrows};
          dataspace_id = H5Screate_simple (1, sdims, NULL);
          /* Create the dataset and write the variable-length string data to it */
          dataset_id = H5Dcreate (file_id, "row_seqid", filetype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
          /* save the char* array */
          H5Dwrite (dataset_id, memtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, seqidqueryh5);
          status = H5Dclose (dataset_id);
          status = H5Sclose (dataspace_id);

          /* third : wordqueryh5 */

          /* if dataset already exist delete it */
          datasetexist = -1;
          H5E_BEGIN_TRY
          datasetexist = H5Lexists(file_id, "row_word", H5P_DEFAULT);
          H5E_END_TRY
          if (datasetexist > 0) {
              H5Ldelete(file_id, "row_word", H5P_DEFAULT);
          }

          /* Create dataspace.  Setting maximum size to NULL sets the maximum size to
          be the current size */
          sdims[0] = (hsize_t)nrows;
          dataspace_id = H5Screate_simple (1, sdims, NULL);
          /* Create the dataset and write the variable-length string data to it */
          dataset_id = H5Dcreate (file_id, "row_word", filetype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
          /* save the char* array */
          H5Dwrite (dataset_id, memtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, wordqueryh5);
          status = H5Dclose (dataset_id);
          status = H5Sclose (dataspace_id);

          /* second : seqidrefh5 */

          /* if dataset already exist delete it */
          datasetexist = -1;
          H5E_BEGIN_TRY
          datasetexist = H5Lexists(file_id, "col_seqid", H5P_DEFAULT);
          H5E_END_TRY
          if (datasetexist > 0) {
              H5Ldelete(file_id, "col_seqid", H5P_DEFAULT);
          }

          /* Create dataspace.  Setting maximum size to NULL sets the maximum size to
          be the current size */
          sdims[0] = (hsize_t)ncols;
          dataspace_id = H5Screate_simple (1, sdims, NULL);
          /* Create the dataset and write the variable-length string data to it */
          dataset_id = H5Dcreate (file_id, "col_seqid", filetype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
          /* save the char* array */
          H5Dwrite (dataset_id, memtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, seqidrefh5);
          status = H5Dclose (dataset_id);
          status = H5Sclose (dataspace_id);


         /* fourth : wordrefh5 */

          /* if dataset already exist delete it */
          datasetexist = -1;
          H5E_BEGIN_TRY
          datasetexist = H5Lexists(file_id, "col_word", H5P_DEFAULT);
          H5E_END_TRY
          if (datasetexist > 0) {
              H5Ldelete(file_id, "col_word", H5P_DEFAULT);
          }

          /* Create dataspace.  Setting maximum size to NULL sets the maximum size to
          be the current size */
          sdims[0] =(hsize_t)ncols;
          dataspace_id = H5Screate_simple (1, sdims, NULL);
          /* Create the dataset and write the variable-length string data to it */
          dataset_id = H5Dcreate (file_id, "col_word", filetype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
          /* save the char* array */
          H5Dwrite (dataset_id, memtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, wordrefh5);
          status = H5Dclose (dataset_id);
          status = H5Sclose (dataspace_id);

          status = H5Tclose (filetype_id);
          status = H5Tclose (memtype_id);
          status = H5Fclose(file_id);
        }
     }
}

void progress_bar(int bloc_calcul, int *bloc_progress, int *calcul_done){
    int bp, cd;
    bp = *bloc_progress;
    cd = *calcul_done;
    *calcul_done = cd + 1;
    *bloc_progress = bp + 1;
    if(bp > bloc_calcul){
        *bloc_progress = 0;
        fprintf(stdout, "\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r%d done", *calcul_done);
        fflush(stdout);
    }
}
