/****************************************
 * File:   diskm
 * Author: jmf
 *
 * Created  mercredi 26 juillet 2023 15:00
 *  Version 0.0.1
 *  Copyright INRAE 2023

This file is part of disseq and mpidisseq.
disseq and mpidisseq are free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */


/*
 * Ce binaire est lancé par diskm_jelly.py
 *
 * Input: k et nk (nombre de kmers réalisés):
 *
 *  Les fichiers générés par diskm_jelly.py:
 *    seqids: fichiers des seqids
 *    basename.kX.jfm (binaire), basename.kX.kmers (texte)
 *    fichiers *.jfas et *.jf
 *
 *  Affiche les résultats sous forme d'un fichier *.dis
 *  (matrice carrée avec nom de lignes et colonnes)
 *    
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
// #include <time.h>
// #include <sys/stat.h>
// #include <dirent.h>
// #include <hdf5.h>

//#define DEBUG
#define MIN(x,y)     ((x)<=(y) ? (x) : (y))

#define timing(a) start=clock(); a; diff = clock() - start; msec = diff * 1000 / CLOCKS_PER_SEC; printf("msecs: %d\n",msec);

unsigned int test_histo(FILE * fp)
{
  /* fp:
   *    k
   *    pour chaque histo:
   *      nmemb
   *      nmemb X pos
   *      nmemb X count
   */

  unsigned int size   = sizeof( unsigned int);
  unsigned int nmemb;
  unsigned int  * item;
  unsigned int n_histo = 0;
  unsigned long int fpos = 0;
  unsigned long int prev_fpos = 0;
  rewind(fp);
  fread(&nmemb,size,1,fp);
  //fprintf(stderr,"kmer %d",nmemb);getchar();

  while(1)
  {
    // à la fin du fichier ftell renvoi toujours la même valeur;
    prev_fpos = ftell(fp);
    fread(&nmemb,size,1,fp);
    printf("nmemb %d\n",nmemb);
    fpos = ftell(fp);
    if (fpos == prev_fpos) break;

    item = calloc(nmemb, sizeof(unsigned int));
    fread(item, size, nmemb, fp);
    printf("pos[0] %d pos[1] %d pos[2] %d\n",item[0], item[1],item[2] );
    /*
    for (unsigned int i=0; i<nmemb; ++i)
    {
      printf("%d\n",item[i]);
    }
    */
    //printf("pos\n");
    //getchar();

    fread(item, size, nmemb, fp);
    printf("count[0] %d count[1] %d count[2] %d\n",item[0], item[1],item[2]);
    /*
    for (unsigned int i=0; i<nmemb; ++i)
    {
      printf("%d\n",item[i]);
    }
    */
    ++n_histo;
    free(item);
  }
  
  --n_histo;
  return n_histo;
}

unsigned int get_histo(FILE * fp,  unsigned int * h_pos,  unsigned int * h_count)
{
  unsigned int k;
  int kpos      = 0;
  int prev_kpos = -1;
  unsigned int count   = 0;
  size_t size   = sizeof( unsigned int);
  size_t nmemb  = 1;
  fread(&nmemb,  size, 1,     fp);
  fread(h_pos,   size, nmemb, fp);
  fread(h_count, size, nmemb, fp);
  if(feof(fp))
  {
    return -1;
  }
  return nmemb;
}

unsigned int distance(unsigned int * h0_pos, unsigned int * h0_count, unsigned int * h1_pos, unsigned int * h1_count, unsigned int h0_l, unsigned int h1_l)
{
  unsigned int d = 0;
  unsigned int k0 = 0, k1 = 0;
/*
  printf("h0_pos\n");
  for (int i=0; i<h_size; ++i)
  {
    printf("%d ",h0_pos[i]);
  }

  printf("h0_count\n");
  for (int i=0; i<h_size; ++i)
  {
    printf("%d ",h0_count[i]);
  }
printf("h1_pos\n");
  for (int i=0; i<h_size; ++i)
  {
    printf("%d ",h1_pos[i]);
  }

  printf("h1_count\n");
  for (int i=0; i<h_size; ++i)
  {
    printf("%d ",h1_count[i]);
  }

getchar();
*/
  while( k0 < h0_l || k1 < h1_l)
  {
    //printf("k0 %d h0_l %d k1 %d h1_l %d d %d\n",k0,h0_l,k1,h1_l,d);//getchar();
    if(h0_pos[k0] == h1_pos[k1])
    {
      d += abs(h0_count[k0] - h1_count[k1]);
      //printf("sync d %d c0 %d c1 %d\n", d,h0_count[k0], h1_count[k1]);
      //getchar();
      ++k0; ++k1;
      continue;
    }
    if (h0_pos[k0] > h1_pos[k1])
    {
      //pprintf("AVANT h0 %d > h1 %d\n", h0_pos[k0],h1_pos[k1]);
      for (;h0_pos[k0] > h1_pos[k1]; ++k1)
      {
        if(k1 > h1_l) 
        {
          //pprintf("OOOOPS k1 %d >= h1_l %d\n",k1,h1_l);
          break; //exit(0);
        };
        //pprintf("k1 %d h0_pos[%d] %d h1_pos[%d] %d\n",k1,k0,h0_pos[k0],k1,h1_pos[k1]);
        //pprintf("h1_count[%d] %d\n",k1, h1_count[k1]);
        d += h1_count[k1];
        //pprintf("d %d\n",d);
        //getchar();
      }
      //pprintf("APRES h0 %d > h1 %d\n", h0_pos[k0],h1_pos[k1]);
      if(h0_pos[k0] < h1_pos[k1])
      {
        d += h0_count[k0];
        //pprintf("h0_count[%d] %d\n",k0, h0_count[k0]);
        //pprintf("d %d\n",d);
        //getchar();
        ++k0;
        if(k0 > h0_l)
        {
          //pprintf("OOOOPS k0 %d >= h0_l %d\n",k0,h0_l);
          break; //exit(0);
        }

        continue;
      }
      ++k0; ++k1;
      continue;
    }
    if (h0_pos[k0] < h1_pos[k1])
    {
      //pprintf("AVANT h0 %d < h1 %d\n", h0_pos[k0],h1_pos[k1]);
      for (;h0_pos[k0] < h1_pos[k1]; ++k0)
      {
        if(k0 > h0_l)
        {
         //p printf("OOOOPS k0 %d >= h0_l %d\n",k0,h0_l);
          break;
          //exit(0);
        }

        //pprintf("k1 %d h0_pos[%d] %d h1_pos[%d] %d\n",k1,k0,h0_pos[k0],k1,h1_pos[k1]);
        //pprintf("h0_count[%d] %d\n",k0, h0_count[k0]);
        d += h0_count[k0];
        //pprintf("d %d\n",d);
        //getchar();
      }
      //pprintf("APRES h0 %d < h1 %d\n", h0_pos[k0],h1_pos[k1]);
      if(h0_pos[k0] > h1_pos[k1])
      {
        d += h1_count[k1];
        //pprintf("h1_count[%d] %d\n",k1, h1_count[k1]);
        //pprintf("d %d\n",d);
        //getchar();
        ++k1;
         if(k1 > h1_l) 
        {
          //pprintf("OOOOPS k1 %d >= h1_l %d\n",k1,h1_l);
          break; //exit(0);
        };
        continue;
      }
      ++k0; ++k1;
      continue;
    }
  }
  //printf("DISTANCE %d\n",d);getchar();
  return d;
}

//void writeHdf5(int nrows, int ncols, int nrowsmpi, int offsetmpi, unsigned int * distances, char outfile[]) {
//
//    hid_t file_id;
//    hid_t plist_id = H5P_DEFAULT;
//
//    /* create a new file collectively */
//    file_id = H5Fcreate(outfile, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
//    herr_t status = H5Pclose(plist_id);
//
//    /* Save the distance matrix */
//
//    /* Dataset must be chunked for compression */
//    hsize_t cdims[2] = {1, ncols};
//    plist_id = H5Pcreate(H5P_DATASET_CREATE);
//    status = H5Pset_chunk(plist_id, 2, cdims);
//
//    /* Set ZLIB / DEFLATE Compression using compression level 5. */
//    status = H5Pset_deflate(plist_id, 5);
//
//    /* Create the data space for the dataset. */
//    hsize_t dims[2] = {nrows, ncols};
//    hid_t dataspace_id = H5Screate_simple(2, dims, NULL);
//
//    /* Create the dataset. */
//    hid_t dataset_id = H5Dcreate2(file_id, "/distances", H5T_IEEE_F32LE,
//                                  dataspace_id, H5P_DEFAULT, plist_id, H5P_DEFAULT);
//    /* Terminate access to properties */
//    status = H5Pclose(plist_id);
//    /* Terminate access to the data space. */
//    status = H5Sclose(dataspace_id);
//
//    hsize_t offset[2], stride[2], count[2], block[2];
//    offset[0] = (hsize_t)offsetmpi;
//    offset[1] = 0;
//    stride[0] = 1;
//    stride[1] = 1;
//    count[0]  = 1;
//    count[1]  = 1;
//    block[0]  = (hsize_t)nrowsmpi;
//    block[1]  = (hsize_t)ncols;
//    /* create memory space */
//    hid_t memspace_id  = H5Screate_simple(2, block, NULL);
//    /* select hyperslab in the file */
//    dataspace_id = H5Dget_space(dataset_id);
//    H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, stride, count, block);
//    plist_id = H5P_DEFAULT;
//    /* Write the dataset. */
//    status = H5Dwrite(dataset_id, H5T_IEEE_F32LE, memspace_id, dataspace_id, plist_id, distances);
//
//    /* End access to the dataset and release resources used by it. */
//    status = H5Dclose(dataset_id);
//    status = H5Sclose(memspace_id);
//    status = H5Sclose(dataspace_id);
//    status = H5Pclose(plist_id);
//    /* Close the file. */
//    status = H5Fclose(file_id);
//}


//https://stackoverflow.com/questions/744766/how-to-compare-ends-of-strings-in-c
int endswith(const char *str, const char *suffix)
{
  size_t str_len = strlen(str);
  size_t suffix_len = strlen(suffix);

  return (str_len >= suffix_len) &&
         (!memcmp(str + str_len - suffix_len, suffix, suffix_len));
}


int read_jf(char * jfile, unsigned char k_oct, unsigned int nk, unsigned int count[])
{
  FILE *fp_i;
  char file[127];
  strcpy(file,jfile);
  strncat(file,".jf",4);
  fp_i = fopen(file, "rb");
  if (fp_i == NULL)
  {
    perror("fopen file");
    printf("%s\n",file);
    getchar();
    return EXIT_FAILURE;
  }
  char offset_c[9]; offset_c[9] = '\0';
  unsigned int offset;
  size_t ret;
  ret = fread(offset_c, sizeof(char), 9, fp_i);
  if (ret != 9)
  {
    perror("fread offset_c");
    return EXIT_FAILURE;
  }

  offset = atol(offset_c);
  char header[offset]; header[offset] = '\0';
  ret = fread(header,sizeof(char), offset, fp_i);
  if (ret != offset)
  {
    printf("ret %zu\n", ret);
    perror("fread offset");
    return EXIT_FAILURE;
  }
  unsigned int kmer; //[16];
  unsigned int c = 0;
  unsigned int n_count = 0;

  for (unsigned int n=0; n<nk; ++n)
  {
    ret = fread(&kmer,  sizeof(char), k_oct, fp_i);
    ret = fread(&c, sizeof(unsigned int), 1, fp_i);
    if (ret == 0) break;
    count[n] = c;
  }
  fclose(fp_i);
}

void *chomp(char *str)
{
  if(str[strlen(str)-1] == 10) str[strlen(str)-1] = 0;
}
 
int main(int argc, char** argv)
{
  if (argc < 2)
  {
    printf("Usage: diskm k nk\n");
    exit(0);
  }
  unsigned char k  = atoi(argv[1]);
  unsigned int  nk = atoi(argv[2]);
  //printf("NK %d\n",nk);
  unsigned char k_oct;
  while(1)
  {
    if      (k <= 4)   { k_oct = 1; break; }
    else if (k <= 8)   { k_oct = 2; break; }
    else if (k <= 16)  { k_oct = 3; break; }
    else if (k <= 32)  { k_oct = 4; break; }
    else if (k <= 64)  { k_oct = 5; break; }
    else if (k <= 128) { k_oct = 6; break; }
    else { printf("k=%hu !! Can't use k > 128",k); exit(0); }
  }
<<<<<<< HEAD
=======
  //printf("k %hhu k_oct %hhu \n", k, k_oct);
  //exit(0);
>>>>>>> ef219134cbfd22920546e9d0682a8ce553ff2d73

  FILE *seql;
  char seqid[127];
  if ((seql = fopen("seqids","r")) == NULL)
  {
    perror("fopen seqids");
    return EXIT_FAILURE;
  }
  //Nb de seqids == nb de lignes
  unsigned int n_seq = 0;
  while (fgets(seqid,127,seql)) ++n_seq;
  rewind(seql);

  char jflist[n_seq][127];
  unsigned int n = 0;
  unsigned int d = 0;
  while (fgets(seqid,127,seql))
  {
    chomp(seqid);
    strcpy(jflist[n],seqid);
<<<<<<< HEAD
=======
    //strncat(jflist[n],".jf",4);
    //printf("jflist[%d] %s\n",n,jflist[n]);
>>>>>>> ef219134cbfd22920546e9d0682a8ce553ff2d73
    ++n;
  }
  unsigned int count_i[nk];
  unsigned int count_j[nk];
<<<<<<< HEAD
  unsigned int d = 0;
=======
>>>>>>> ef219134cbfd22920546e9d0682a8ce553ff2d73
  unsigned int * distances = (unsigned int *) calloc(n_seq * n_seq, sizeof(unsigned int));

  for (unsigned int i=0; i<n_seq; ++i)
  {
    printf("%s",jflist[i]);
    read_jf(jflist[i], k_oct, nk, count_i);
    distances[i * n_seq + i] = 0;
    for (unsigned int j=i+1; j<n_seq; ++j)
    {
      read_jf(jflist[j], k_oct, nk, count_j);
      d = 0;
      for (unsigned int k=0; k<nk; ++k)
      {
        d += abs(count_i[k] - count_j[k]);
      }
      distances[i * n_seq + j] = d;
      distances[j * n_seq + i] = d;
    }
    for (unsigned int col=0; col<n_seq; ++col)
    {
      printf("\t%d",distances[i * n_seq + col]);
    }
    printf("\n");
  }
}

  /*
  for (int i=0; i<n; ++i)
  {
    printf("%s\n",seqlist[i]);
    fread(offset_c, sizeof(char), 9, fp_i);
    unsigned int offset = atol(offset_c);// + 9;
    printf("%d\n", offset);
  }
  */
 /* 
  unsigned char k     = 3;
  unsigned char k_oct = 1; 
  char jFile[127] = "10A10C_k3.jf";//"10A_k3.jf";"10A10C_k5.jf";
  FILE *fp_j      = fopen(jFile, "rb");
  if (!fp_j) {
    perror("fopen");
    return EXIT_FAILURE;
  }
  size_t ret;
  char offset_c[9]; offset_c[9] = '\0';
  fread(offset_c, sizeof(char), 9, fp_j);
  unsigned int offset = atol(offset_c);// + 9;
  printf("%d\n", offset);
  char header[offset]; header[offset] = '\0';
  fread(header,sizeof(char), offset, fp_j);
  printf("%s\n", header);
  printf("offset %u\n", offset);
  unsigned int kmer; //[16];
  unsigned int count;
  while(1)
  {
    fread(&kmer,  sizeof(char),         k_oct, fp_j);
    fread(&count, sizeof(unsigned int),     1, fp_j);
    if (feof(fp_j) != 0) break;
    printf("kmer: %hhu count %u\n",kmer, count);
  }

}*/


// int main(int argc, char** argv)
// {
// 
//   //printf("NOT RUNNING !!!!!"); exit(0);
//   char queryFile[127];
//   char refFile[127];
//   char revFile[127];
//   char masterFile[127];
//   char outFile[127];
// 
//   sprintf(queryFile, "%s.jfkm", argv[1]);
//   sprintf(refFile, "%s", queryFile);
//   sprintf(revFile, "%s_rev.jfkm", argv[1]);
//   sprintf(masterFile, "%s.master.jfkm", argv[1]);
//   sprintf(outFile, "%s.dkm", argv[1]);
// 
//   FILE *fp_query     = fopen(queryFile, "r");
//   FILE *fp_rev_query = fopen(revFile, "r");
//   FILE *fp_ref       = fopen(refFile,   "r");
//   FILE *out          = fopen(outFile, "w");
//   
//   if (! fp_ref)
//   {
//     fprintf(stderr,"%s: ",argv[1]);
//     perror("");
//     return EXIT_FAILURE;
//   }
// /* Test histogramme
//   unsigned int n_histo;
//   n_histo = test_histo(fp_ref);
//   printf("n_histo %d\n",n_histo);
//   exit(0);
// */
//   
//   unsigned int n_ref, n_query, k_ref, k_query, n_rev_query, k_rev_query;
//   fread(&n_query,sizeof(unsigned int),1,fp_query);
//   fread(&n_rev_query,sizeof(unsigned int),1,fp_rev_query);
//   fread(&n_ref,sizeof(unsigned int),1,fp_ref);
// 
//   fread(&k_query,sizeof(unsigned int),1,fp_query);
//   fread(&k_rev_query,sizeof(unsigned int),1,fp_rev_query);
//   fread(&k_ref,sizeof(unsigned int),1,fp_ref);
//   if(k_query != k_ref)
//   {
//     printf("!!!!! k_query %d != k_ref %d !!!!!\n",k_query,k_ref);
//     exit(1);
//   }
//   printf("n_query %d n_ref %d\n",n_query, n_ref);
//   printf("n_rev_query %d \n",n_rev_query);
//   //master (les kmers réalisés)
//   struct stat filestat;
//   stat(masterFile, &filestat);
//   unsigned int n_all_k = filestat.st_size / k_ref;
//   unsigned int * h0_pos;
//   unsigned int * h0_count;
//   unsigned int * h0_rev_pos;
//   unsigned int * h0_rev_count;
//   unsigned int * h1_pos;
//   unsigned int * h1_count;
//   
//   h0_pos       = calloc(n_all_k, sizeof( unsigned int));
//   h0_count     = calloc(n_all_k, sizeof( unsigned int));
//   h0_rev_pos   = calloc(n_all_k, sizeof( unsigned int));
//   h0_rev_count = calloc(n_all_k, sizeof( unsigned int));
//   h1_pos       = calloc(n_all_k, sizeof( unsigned int));
//   h1_count     = calloc(n_all_k, sizeof( unsigned int));
//   
//   unsigned int d, d_rev;
//   unsigned int h0_l = 0, h0_rev_l = 0, h1_l = 0;
//   unsigned long h1_fpos = 0;
//   int nq = 0, nr = 0;
// 
//   // 1 vector d(i,j) ==> distances[i * n_ref + j]
//   unsigned int * distances = (unsigned int *) calloc(n_query * n_ref, sizeof(unsigned int));
//   if (strcmp(queryFile, refFile) == 0) // 2 fois le même fichier d'entrée
//   {
// 
//      // Calcule diag sup, écrit tout
//       for (unsigned int i=0; i<n_query;++i)
//       {
//         h0_l     = get_histo(fp_query,    h0_pos,    h0_count);
//         h0_rev_l = get_histo(fp_rev_query,h0_rev_pos,h0_rev_count);
//         h1_l     = get_histo(fp_ref, h1_pos, h1_count);
//         if(h0_l == -1) break;
//         h1_fpos = ftell(fp_ref);
//         for (unsigned int j=i+1; j<n_ref; ++j)
//         {
//           h1_l = get_histo(fp_ref,h1_pos,h1_count);
//           if(h1_l == -1) break;   
//           d     = distance(h0_pos, h0_count, h1_pos, h1_count, h0_l, h1_l);
//           if(h0_rev_l != -1) //pas de revcomp
//           {
//             d_rev =  distance(h0_rev_pos, h0_rev_count, h1_pos, h1_count, h0_rev_l, h1_l);
//             d = MIN(d, d_rev);
//           }
//           distances[i * n_ref + j] = d;
//           distances[j * n_ref + i] = d;
//         }
//         fseek(fp_ref,h1_fpos,SEEK_SET);
//       }
//   }
//   else  
//   {
//     // ALL vs ALL NON IMPLEMENTÉ
//     for (unsigned int i=0; i<n_query;++i)
//       {
//         h0_l = get_histo(fp_query,h0_pos,h0_count);
//         if(h0_l == -1) break;
//         for (unsigned int j=0; j<n_ref;++j)
//         {
//           h1_l = get_histo(fp_ref,h1_pos,h1_count);
//           if(h1_l == -1) break;   
//           d = distance(h0_pos, h0_count, h1_pos, h1_count, h0_l, h1_l);
//           distances[i * n_ref + j] = d;
//           distances[j * n_ref + i] = d;
//         }
//         fseek(fp_ref,0,SEEK_SET);
//       }   
//   }
//   fwrite(&n_query, sizeof(unsigned int), 1,                out);
//   fwrite(&n_ref,   sizeof(unsigned int), 1,                out);
//   fwrite(distances,sizeof(unsigned int), n_query *  n_ref, out);
// 
// /*
//   if((argc == 3 && strcmp(argv[2], "dis") == 0))
//   {
//     printf("Yeaah ascii\n");
//   }
// */
// /*support hdf5 eliminé
//   if (argc == 6)
//   {
//     writeHdf5(n_query, n_ref, n_ref, 0, distances, argv[5]);
//   }
//   */
//   fclose(fp_query);
//   fclose(fp_ref);
//   free(distances);
//   fclose(out);
//   free(h0_pos);
//   free(h0_count);
//   free(h1_pos);
//   free(h1_count);
// }
