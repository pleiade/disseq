#===========================================================================
# MPIdisedi executable
#===========================================================================
if (MPIDISEDI)
   add_executable(mpidisedi mpidisedi.c func.c)
   target_link_libraries(mpidisedi common MPI::MPI_C)
 endif(MPIDISEDI)

 if (MPIDISEDI_SPARSE)
   add_executable(mpidisedi_sparse mpidisedi_sparse.c func.c)
   target_link_libraries(mpidisedi_sparse common MPI::MPI_C)
   install(TARGETS mpidisedi_sparse DESTINATION bin)   
 endif(MPIdisedi_SPARSE)
