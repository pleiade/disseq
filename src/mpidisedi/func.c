/*
 * File:   func.c
 * Author: jmf
 *
 * Created on 7 octobre 2014
 * Copyright INRAE 2022

This file is part of disseq.
disseq is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */

#ifndef FUNC_C
#define FUNC_C

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <mpi.h>
#include <ctype.h>

#include "com_param.h"
#include "com_func.h"
#include "param.h"
#include "func.h"

int load_arguments(int argc, char **argv, char *queryFile, char *refFile, char *outFile, int *dismax)
{
  for( int a=1; a < argc; a+=2 )
  {
    if (strncmp(argv[a], "-ref",4) == 0)
      strncpy(refFile, argv[a + 1], MAXFILENAME);
    else if (strncmp(argv[a], "-query",6) == 0)
     strncpy(queryFile, argv[a + 1], MAXFILENAME);
    else if (strncmp(argv[a], "-out",4) == 0)
      strncpy(outFile,argv[a + 1], MAXFILENAME);
    else  if (strncmp(argv[a], "-dismax",7) == 0)
        *dismax = strtol(argv[a + 1], NULL, 10);
    else
      printf("Unknown argument %s\n",argv[a]);
  }
  return 0;
}

int load_pairs_arguments(int argc, char **argv, char *pairFile, char *queryFile, char *refFile, char *outFile, float *dismax)
{
  for( int a=1; a < argc; a+=2 )
  {
    if (strncmp(argv[a], "-pair", 5) == 0)
      strncpy(pairFile, argv[a + 1], MAXFILENAME);
    else if (strncmp(argv[a], "-ref",4) == 0)
      strncpy(refFile, argv[a + 1], MAXFILENAME);
    else if (strncmp(argv[a], "-query",6) == 0)
     strncpy(queryFile, argv[a + 1], MAXFILENAME);
    else if (strncmp(argv[a], "-out",4) == 0)
      strncpy(outFile,argv[a + 1], MAXFILENAME);
    else  if (strncmp(argv[a], "-dismax",7) == 0)
        *dismax = strtol(argv[a + 1], NULL, 10);
    else
      printf("Unknown argument %s\n",argv[a]);
  }
  return 0;
}


int get_fasta_infos_with_pos(FILE *fh, int *nSeq, int *maxseqlen, int *maxidlen, long int **seqPos)
{
  fseek(fh, 0L, SEEK_SET);
  long int fpos = 0;
  int nseq = 0;
  char testligne[MAXSEQLEN];
  char *saveptr1, *token, *delim;
  token = calloc((MAXSEQLEN +1),sizeof(char) );
  int seqLen;
  while(fgets(testligne, MAXSEQLEN, fh) != NULL)
  {
    chomp(testligne);
    if(testligne[0] == '>')
    {
      seqLen = 0;
      fpos=ftell(fh) -1; //because of chomp
      fpos -= strlen(testligne);

      //Eliminate ' '
       delim = " ";
       token = strtok_r(testligne,delim,&saveptr1);

       //Eliminate '\t'
      delim = "\t";
      token = strtok_r(token,delim,&saveptr1);
      strncpy(testligne,token, strlen(token)+1);
      if(strlen(testligne)-1 > *maxidlen) *maxidlen=strlen(testligne)-1;
      *seqPos=realloc(*seqPos,++nseq*sizeof(long int));
      (*seqPos)[nseq-1] = fpos;
    }
    else
    {
      seqLen += strlen(testligne);
    }
    *maxseqlen = MAX2(*maxseqlen,seqLen);
  }
  *nSeq = nseq;
  return 0;
}

int get_fasta_infos(FILE *fh, int *nSeq, int *maxseqlen, int *maxidlen)
{
  fseek(fh, 0L, SEEK_SET);
  long int fpos = 0;
  *nSeq = 0;
  char testligne[MAXSEQLEN];
  char *saveptr1, *token, *delim;
  token = calloc((MAXSEQLEN +1),sizeof(char) );
  int seqLen;
  while(fgets(testligne, MAXSEQLEN, fh) != NULL)
  {
    chomp(testligne);
    if(testligne[0] == '>')
    {
      seqLen = 0;
      //Eliminate ' '
       delim = " ";
       token = strtok_r(testligne,delim,&saveptr1);

       //Eliminate '\t'
      delim = "\t";
      token = strtok_r(token,delim,&saveptr1);
      strncpy(testligne,token, strlen(token)+1);
      if(strlen(testligne)-1 > *maxidlen) *maxidlen=strlen(testligne)-1;
      ++(*nSeq);
    }
    else
    {
      seqLen += strlen(testligne);
    }
    *maxseqlen = MAX2(*maxseqlen,seqLen);
  }
  return 0;
}

int read_all_ref_seq(char * refFile, struct Oneseq *refseqlist, int maxseqlen, int maxidlen, int nRefseqTotal, MPI_Comm comm)
{
  FILE *fh;
  long int fsize;
  int err = 0;

  struct Oneseq oneseqref;
  oneseqref.id   = calloc((maxidlen +1), sizeof(char) );
  oneseqref.dna  = calloc((maxseqlen +1),sizeof(char) );//free
  oneseqref.rdna = calloc((maxseqlen +1),sizeof(char) );//free
  int nRefseq = 0;
  if (( fh = fopen( refFile , "r")) == NULL)
  {
    fprintf(stderr,"Can't open %s\n",refFile);
    MPI_Abort(comm,err);
  }
  while ( nRefseq < nRefseqTotal )
  {
    next_seq(fh, &oneseqref, maxseqlen,maxidlen);
    strncpy(refseqlist[nRefseq].id,oneseqref.id,strlen(oneseqref.id)+1);
    refseqlist[nRefseq].len = strlen(oneseqref.dna);
    strncpy(refseqlist[nRefseq].dna,oneseqref.dna,strlen(oneseqref.dna)+1);
    reverse_complement(refseqlist[nRefseq]); //from Commun_func
    nRefseq++;
   }
  fclose(fh);

  free(oneseqref.rdna);
  free(oneseqref.dna);
  free(oneseqref.id);
  return 0;
}

char * chomp(char *str) //in place
{
  if (str && *str && str[strlen(str)-1]=='\n') str[strlen(str)-1]=0;
  return str;
}

int next_seq(FILE *fh_query, struct Oneseq *queryseq, int maxseqlen, int maxidlen)
{
  maxidlen += 2; // > and \n
  char id[maxidlen];    id[0]='\0';
  char seq[maxseqlen];  seq[0]='\0';
  char line[maxseqlen]; line[0]='\0';

  int next='\0';
  char *saveptr1, *token, *delim;
  int indicefinchaine=0;
  token = calloc((maxseqlen),sizeof(char) );
  while(fgets(line,maxseqlen,fh_query) != NULL)
  {
    chomp(line);
    next = fgetc(fh_query);
    ungetc(next,fh_query);

    if(line[0] == '>')
    {
       //Eliminate '>'
       delim = ">";
       token[0]='\0';
       token = strtok_r(line,delim,&saveptr1);

       //Eliminate ' '
       delim = " ";
       token = strtok_r(token,delim,&saveptr1);

      //Eliminate '\r'
       delim = "\r";
       token = strtok_r(token,delim,&saveptr1);

       //Eliminate '\t'
       delim = "\t";
       token = strtok_r(token,delim,&saveptr1);
       queryseq->id[0]='\0';
       strncpy(queryseq->id,token, strlen(token)+1);
    }
    else
    {
      strncat(seq,line,strlen(line)+1);
    }
    if (next == '>' || next == EOF)
    {
      queryseq->dna[0]  = '\0';
      indicefinchaine=strlen(seq)+1;
      seq[indicefinchaine]='\0';
      convertToUpperCase(seq);
      strncpy(queryseq->dna,seq,strlen(seq)+1);
      queryseq->len = strlen(seq);
      break;
    }
  }
  return 0;
}

#endif // FUNC_C
