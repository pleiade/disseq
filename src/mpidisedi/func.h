/* 
 * File:   func.h
 * Author: jmf
 *
 * Created on 7 octobre 2014
 * Copyright INRAE 2022

This file is part of disseq.
disseq is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */

#ifndef FUNC_H
#define	FUNC_H


int get_max_id_len(long int offset_deb, long int offset_fin, char *pairFile, int nProc, int rang, MPI_Comm comm,
int  *maxidlen, int * nPair);
int gotoEndOfLine(FILE *fh, long int fsize);
int readchar(FILE *fh, long int fsize);
int readchar_all(FILE *fh, long int fsize);
int read_id(FILE *fh, char * id, long int fsize);

char * chomp(char *str);

int next_seq(FILE *fh_query, struct Oneseq *queryseq, int maxseqlen, int maxidlen);

int get_fasta_infos(FILE *fh, int *nSeq, int *maxseqlen, int *maxidlen);

int get_fasta_infos_with_pos(FILE *fh, int *nSeq, int *maxseqlen, int *maxidlen,long int ** seqPos);

int seqIndex(FILE * fh, long int ** qSeq);

int load_arguments(int argc, char **argv, char *queryFile, char *refFile, char *outFile, int *dismax);

int load_pairs_arguments(int argc, char **argv, char *pairFile, char *queryFile, char *refFile, char *outFile, float *dismax);

int valid_file_input_size(char *inputFile, int nProc, int rang, int *total_nb_line, MPI_Comm comm);

int read_up_to_tab(FILE *fh, char *id);

int read_all_pairs(long int offset_deb, long int offset_fin, char * pairFile, struct Paire *paire, int maxidlen, int *nQuery, int *nCalc , MPI_Comm comm);

int read_next_ref_seq(FILE *fh, char * id, char * seq, long int fsize);

int read_all_ref_seq(char * refFile, struct Oneseq *refseqlist, int maxseqlen, int maxidlen, int nRefseq, MPI_Comm comm);

int get_seq_from_list(int p,struct Paire *paire, struct Oneseq *refseqlist, struct Oneseq *refseq);

int get_seq_from_file(FILE *fh, long int fsize, char * query, struct Oneseq *queryseq, int maxidlen, int maxseqlen, int rang);

int get_max_seq_len(char *fastaFile, int nProc, int rang,  MPI_Comm comm,
                    int * maxseqlen, int * nRefseq);

#endif //FUNC_H
