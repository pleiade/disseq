/****************************************
 * File:   mpidisseq/main.c
 * Author: jmf
 *
 * Created vendredi 27 juin 2014 14:16 
 *  Version 0.0.1
 *  IDRIS (Sylvie Thérond) 8 avril 2015
 *  Version 1.0.0
 ****************************************
 * 16/04/2015
 * File:   mpidisseq/mpidisseqAll_sparse.c
 *
 *
 * This file is part of disseq.
disseq is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 */



#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "com_param.h"
#include "com_func.h"
#include "param.h" //PARAM_H
#include "func.h" //FUNC_H


int main(int argc, char** argv)
{
  int rang, nProc,err = 1;
  MPI_Init(&argc, &argv);
  MPI_Comm comm;
  MPI_Status status;
  comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm, &rang);
  MPI_Comm_size(comm, &nProc);
  FILE *fh_query, *fh_ref;
  long int fsize;
  char refFile[MAXFILENAME];     refFile[0]     = '\0';
  char queryFile[MAXFILENAME];   queryFile[0]   = '\0';
  char outFile[MAXFILENAME + 20];     outFile[0]     = '\0';
  int dismax = 30;
  if (argc < 4 )
  {
    if (rang == 0) printf("Usage:\n mpiexec -np 2 mpidisedi_sparse \
-ref R.fas -query Q.fas [-out outFile (default RESULT) \
-dismax 20 (default 30)]\n");
    MPI_Finalize();
    exit(0);
  }
  load_arguments(argc, argv, queryFile, refFile, outFile, NULL);

  int maxseqlen = 0;//MAXSEQLEN;
  int maxidlen  = 0;//MAXIDLEN;

  if (( fh_query = fopen( queryFile , "r" ) ) == NULL)
  {
    fprintf(stderr, "Can't open queryFile %s\n",queryFile);
    MPI_Abort(comm,err);
  }
  if (( fh_ref = fopen( refFile , "r" ) ) == NULL)
  {
    fprintf(stderr, "Can't open refFile %s\n",refFile);
    MPI_Abort(comm,err);
  }

  int nQuery;
  int nRefseq;
  long int *seqPos = NULL;
  get_fasta_infos_with_pos(fh_query,&nQuery,&maxseqlen,&maxidlen,&seqPos);
  int maxseqlenq  = maxseqlen;
  int maxidlenq   = maxidlen;
  get_fasta_infos(fh_ref,&nRefseq,&maxseqlen,&maxidlen);
  maxseqlen       = MAX2(maxseqlenq,maxseqlen);
  maxidlen        = MAX2(maxidlenq,maxidlen);

  //Share the work
  if (nQuery <  nProc)
  {
     fprintf(stderr, "Too few queries (%d) for too many procs (%d)\n",nQuery,nProc);
     MPI_Abort(comm,1);
  }

  int by_proc = nQuery/nProc;
  if (nQuery % nProc > rang) by_proc++;
  
  int suivant   = (rang + 1) % nProc;
  int precedent = (nProc + rang - 1) % nProc;

  int first_seq = 0;
  int last_seq  = by_proc - 1;

// Ring communication
  if (rang == 0)
  {
    MPI_Send(&last_seq,1,MPI_INT,suivant,1,comm);
    MPI_Recv(&first_seq,1,MPI_INT,precedent,1,comm,&status);
    first_seq = 0;
    last_seq  = by_proc - 1;
  }
  else
  {
    MPI_Recv(&first_seq,1,MPI_INT,precedent,1,comm,&status);
    last_seq = first_seq + by_proc;
    first_seq += 1;
    MPI_Send(&last_seq,1,MPI_INT,suivant,1,comm);
  }


  struct Oneseq *refseqlist = NULL;
  refseqlist = calloc((nRefseq+1),sizeof (struct Oneseq));
  for(int i=0; i < nRefseq; ++i)
  {
    refseqlist[i].id    = calloc((maxidlen +1 ), sizeof(char));
    refseqlist[i].dna   = calloc((maxseqlen +1), sizeof(char));
    refseqlist[i].rdna  = calloc((maxseqlen +1), sizeof(char));
  }

  read_all_ref_seq(refFile,refseqlist,maxseqlen,maxidlen,nRefseq,comm);

  int nCalc     = 0;
  struct Oneseq queryseq;
  queryseq.id   = calloc((maxidlen +1), sizeof(char) );
  queryseq.dna  = calloc((maxseqlen +1),sizeof(char) );//free
  queryseq.rdna = calloc((maxseqlen +1),sizeof(char) );// Not used
  float my_dist = 0;

  //création de la matrice SW
  float **SW = NULL;
  SW = alloc_NW_matrix(maxseqlen+1); //from Commun_func

  //création de la matrice comp_Matrix
  float **comp_Matrix = NULL;
  comp_Matrix = build_comp_matrix();//from Commun_func
  
  if (strlen(outFile) == 0)
    sprintf(outFile,"RESULT.mpidisedi_sparse.%d",rang);
  else
    sprintf(outFile,"%s.%d",outFile,rang);

  FILE *outfh;
  if( (outfh = fopen(outFile,"w") ) == NULL)
  {
    fprintf(stderr, "Can't open %s\n",outFile);
  }
  if (rang == 0) fprintf(stderr,"Results in %s [0-%d]\n", outFile,nProc -1);
  int ndist = 0;
  for (int p = first_seq; p <= last_seq; ++p)
  {
    fseek(fh_query,seqPos[p], SEEK_SET);
    next_seq(fh_query,&queryseq,maxseqlen,maxidlen);
    for (int r = 0; r <  nRefseq; ++r)
    {
      my_dist = sw_distance(queryseq, refseqlist[r], SW,comp_Matrix);//from Commun_func
      if (my_dist <= dismax)
        fprintf(outfh,"%s\t%s\t%0.2f\n",queryseq.id,refseqlist[r].id,my_dist);
      ++ndist;
    }
  }
  //CLOSE FILES
  fclose(fh_query);
  fclose(fh_ref);
  fclose(outfh);

  free(queryseq.id);
  free(queryseq.dna);
  free(queryseq.rdna);
   
  for (int i = 0; i < nRefseq; ++i )
  {
    free(refseqlist[i].id );
    free(refseqlist[i].dna);
    free(refseqlist[i].rdna);
    refseqlist[i].id = refseqlist[i].dna = refseqlist[i].rdna = NULL;
  }
  free(refseqlist);
  refseqlist    =  NULL;
  queryseq.id   =  NULL;
  queryseq.dna  =  NULL;
  SW            =  NULL;
  comp_Matrix   =  NULL;


  MPI_Finalize();
  return (EXIT_SUCCESS);
}

