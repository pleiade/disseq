/****************************************
 * File:   diskm
 * Author: jmf
 *
 * Created  mercredi 26 juillet 2023 15:00
 *  Version 0.0.1
 *  Copyright INRAE 2023

This file is part of disseq and mpidisseq.
disseq and mpidisseq are free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */


/*
 * Ce binaire est lancé par mpidiskm_jelly.py
 *
 * Input: k et nk (nombre de kmers réalisés):
 *
 *  Les fichiers générés par mpidiskm_jelly.py:
 *    seqids: fichiers des seqids
 *    fichiers *.jfas et *.jf
 *
 *  Sauvegarde les résultats sous forme d'un fichier *.h5
 *  (dataset /distances)
 *    
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <ctype.h>
#include <mpi.h>
#include <hdf5.h>
#include <math.h>

// #include <time.h>
// #include <sys/stat.h>
// #include <dirent.h>
// #include <hdf5.h>

//#define DEBUG
#define MIN(x,y)     ((x)<=(y) ? (x) : (y))

#define timing(a) start=clock(); a; diff = clock() - start; msec = diff * 1000 / CLOCKS_PER_SEC; printf("msecs: %d\n",msec);


void writeHdf5(int nrows, int ncols, int nrowsmpi, int offsetmpi, unsigned int * distances, char outfile[]) {

    hid_t file_id;
    hid_t plist_id = H5P_DEFAULT;

    /* create a new file collectively */
    file_id = H5Fcreate(outfile, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    herr_t status = H5Pclose(plist_id);

    /* Save the distance matrix */

    /* Dataset must be chunked for compression */
    hsize_t cdims[2] = {1, ncols};
    plist_id = H5Pcreate(H5P_DATASET_CREATE);
    status = H5Pset_chunk(plist_id, 2, cdims);

    /* Set ZLIB / DEFLATE Compression using compression level 5. */
    status = H5Pset_deflate(plist_id, 5);

    /* Create the data space for the dataset. */
    hsize_t dims[2] = {nrows, ncols};
    hid_t dataspace_id = H5Screate_simple(2, dims, NULL);

    /* Create the dataset. 
    hid_t dataset_id = H5Dcreate2(file_id, "/distances", H5T_IEEE_F32LE,
                                  dataspace_id, H5P_DEFAULT, plist_id, H5P_DEFAULT);
    */
    hid_t dataset_id = H5Dcreate2(file_id, "/distances", H5T_STD_I32LE,
                                  dataspace_id, H5P_DEFAULT, plist_id, H5P_DEFAULT);
    /* Terminate access to properties */
    status = H5Pclose(plist_id);
    /* Terminate access to the data space. */
    status = H5Sclose(dataspace_id);

    hsize_t offset[2], stride[2], count[2], block[2];
    offset[0] = (hsize_t)offsetmpi;
    offset[1] = 0;
    stride[0] = 1;
    stride[1] = 1;
    count[0]  = 1;
    count[1]  = 1;
    block[0]  = (hsize_t)nrowsmpi;
    block[1]  = (hsize_t)ncols;
    /* create memory space */
    hid_t memspace_id  = H5Screate_simple(2, block, NULL);
    /* select hyperslab in the file */
    dataspace_id = H5Dget_space(dataset_id);
    H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, stride, count, block);
    plist_id = H5P_DEFAULT;
    /* Write the dataset. */
    //status = H5Dwrite(dataset_id, H5T_IEEE_F32LE, memspace_id, dataspace_id, plist_id, distances);
    status = H5Dwrite(dataset_id, H5T_STD_I32LE, memspace_id, dataspace_id, plist_id, distances);

    /* End access to the dataset and release resources used by it. */
    status = H5Dclose(dataset_id);
    status = H5Sclose(memspace_id);
    status = H5Sclose(dataspace_id);
    status = H5Pclose(plist_id);
    /* Close the file. */
    status = H5Fclose(file_id);
}


//https://stackoverflow.com/questions/744766/how-to-compare-ends-of-strings-in-c
int endswith(const char *str, const char *suffix)
{
  size_t str_len = strlen(str);
  size_t suffix_len = strlen(suffix);

  return (str_len >= suffix_len) &&
         (!memcmp(str + str_len - suffix_len, suffix, suffix_len));
}


int read_jf(char * jfile, unsigned char k_oct, unsigned int nk, unsigned int count[])
{
  FILE *fp_i;
  char file[127];
  strcpy(file,jfile);
  strncat(file,".jf",4);
  fp_i = fopen(file, "rb");
  if (fp_i == NULL)
  {
    perror("fopen file");
    //printf("%s\n",file);
    getchar();
    return EXIT_FAILURE;
  }
  char offset_c[9]; offset_c[9] = '\0';
  unsigned int offset;
  size_t ret;
  ret = fread(offset_c, sizeof(char), 9, fp_i);
  if (ret != 9)
  {
    perror("fread offset_c");
    return EXIT_FAILURE;
  }

  offset = atol(offset_c);
  char header[offset]; header[offset] = '\0';
  ret = fread(header,sizeof(char), offset, fp_i);
  if (ret != offset)
  {
    //printf("ret %zu\n", ret);
    perror("fread offset");
    return EXIT_FAILURE;
  }
  unsigned int kmer; //[16];
  unsigned int c = 0;
  unsigned int n_count = 0;

  for (unsigned int n=0; n<nk; ++n)
  {
    ret = fread(&kmer,  sizeof(char), k_oct, fp_i);
    ret = fread(&c, sizeof(unsigned int), 1, fp_i);
    if (ret == 0) break;
    count[n] = c;
  }
  fclose(fp_i);
}

void *chomp(char *str)
{
  if(str[strlen(str)-1] == 10) str[strlen(str)-1] = 0;
}
 
int main(int argc, char** argv)
{
  int rank, nProc, err = 1;
  MPI_Init(&argc, &argv);
  MPI_Comm comm;
  MPI_Status status;
  comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &nProc);

  if (argc < 3)
  {
    if (rank == 0)    printf("Usage: mpidiskm k nk\n");
    MPI_Finalize();
    exit(0);
  } 

  unsigned char k  = atoi(argv[1]);
  unsigned int  nk = atoi(argv[2]);
  unsigned char k_oct;
  while(1)
  {
    if      (k <= 4)   { k_oct = 1; break; }
    else if (k <= 8)   { k_oct = 2; break; }
    else if (k <= 16)  { k_oct = 3; break; }
    else if (k <= 32)  { k_oct = 4; break; }
    else if (k <= 64)  { k_oct = 5; break; }
    else if (k <= 128) { k_oct = 6; break; }
    else { printf("k=%hu !! Can't use k > 128",k);MPI_Finalize(); exit(0); }
  }

  FILE *seql;
  char seqid[127];
  if ((seql = fopen("seqids","r")) == NULL)
  {
    perror("fopen seqids");
    return EXIT_FAILURE;
  }
  //Nb de seqids == nb de lignes
  unsigned long int n_seq = 0;
  while (fgets(seqid,127,seql)) ++n_seq;
  rewind(seql);

  unsigned long int nJobs = 0;
  nJobs = (n_seq * (n_seq -1)) / 2;
  unsigned long int byProc = nJobs / nProc;
  if (nJobs % nProc > rank) ++byProc;
  unsigned long int nextJob  = (rank + 1) % nProc;
  unsigned long int prevJob  = (nProc + rank - 1) % nProc;
//fprintf(stderr,"%d: prevJob %lu nextJob %lu\n",rank,prevJob,nextJob);
    unsigned long int firstJob; 
    unsigned long int lastJob;

  // Ring communication
  if (rank == 0)
  {
    firstJob = 1;
    lastJob  = byProc;
    MPI_Send(&lastJob,1,MPI_LONG,nextJob,1,comm);
    MPI_Recv(&firstJob,1,MPI_LONG,prevJob,1,comm,MPI_STATUS_IGNORE);
    firstJob = 1;
    lastJob  = byProc;
  }
  else
  {
    MPI_Recv(&firstJob,1,MPI_LONG,prevJob,1,comm,MPI_STATUS_IGNORE);
    lastJob = firstJob + byProc;
    firstJob += 1;
    MPI_Send(&lastJob,1,MPI_LONG,nextJob,1,comm);
  }

  unsigned long int first_i = 0;
  unsigned long int first_j = 0;
  unsigned long int b = (2 * n_seq - 1);
  unsigned long int delta = pow(b,2) - 8 * (firstJob + 1);
  first_i = ceil((b - sqrt(delta))/2 -1);
  first_j = (unsigned long int)(firstJob) + first_i - n_seq * (n_seq-1)/2 +
            (n_seq - first_i)*((n_seq - first_i)-1)/2;

//fprintf(stderr,"%d: nJobs %lu firstJob %lu lastJob %lu b %lu delta %lu first_i %lu first_j %lu\n", rank, nJobs, firstJob, lastJob, b, delta,first_i, first_j);

  char jflist[n_seq][127];
  unsigned int n = 0;
  while (fgets(seqid,127,seql))
  {
    chomp(seqid);
    strcpy(jflist[n],seqid);
    ++n;
  }
  unsigned int count_i[nk];
  unsigned int count_j[nk];
  unsigned int d = 0;
  unsigned int * distances = (unsigned int *) calloc(byProc, sizeof(unsigned int));
  unsigned long int d_i = 0;

  read_jf(jflist[first_i], k_oct, nk, count_i);
  for (unsigned int job=firstJob; job<lastJob; ++job)
  {
    read_jf(jflist[first_j], k_oct, nk, count_j);
    d = 0;
    for (unsigned int k=0; k<nk; ++k)
    {
      d += abs(count_i[k] - count_j[k]);
    }
    distances[d_i++] = d;
    ++first_j;
    if (first_j == n_seq)
    {
      ++first_i;
      first_j = first_i + 1;
      read_jf(jflist[first_i], k_oct, nk, count_i);
    }
  }
  d = 0;
  for (unsigned int k=0; k<nk; ++k)
  {
    d += abs(count_i[k] - count_j[k]);
  }
  distances[d_i++] = d;
//fprintf(stderr,"%d: byProc %lu d[0] %d - d[%lu] %d\n",rank, byProc, distances[0], byProc - 1, distances[byProc - 1]);
  int recvcounts[nProc];
  MPI_Allgather(&byProc,1,MPI_INT,recvcounts,1,MPI_INT,comm);

  // Reduce
  if (rank == 0)
  {
    unsigned int * n_distances = (unsigned int *) calloc(n_seq * n_seq,
                                 sizeof(unsigned int));
    unsigned int * p_distances = (unsigned int *) calloc(byProc,
                                 sizeof(unsigned int));
    unsigned long int n_index = 1;
    unsigned long int d_i = 0;
    unsigned long int d_j = 0;

    for ( unsigned long int d_index=0; d_index<byProc; ++d_index )
    {
      n_distances[n_index++] = distances[d_index];

      //Avant i == j
      if(n_index != 0 && n_index % n_seq == 0)
      {
        ++d_i;
        for (d_j = 0; d_j<d_i; ++d_j)
        {
          n_distances[n_index++] = n_distances[d_j * n_seq + d_i];
        }
        n_distances[n_index++] = 0;
      }
    }
    
    for (int p=1; p<nProc; ++p)
    {
      MPI_Recv(p_distances,recvcounts[rank],MPI_INT,p,123,comm,&status);
      for ( unsigned long int d_index=0; d_index<recvcounts[p]; ++d_index )
      {
        n_distances[n_index++] = p_distances[d_index];
        if((n_index != 0) && (n_index % n_seq == 0))
         {
          ++d_i;
          for (d_j = 0; d_j<d_i; ++d_j)
          {
            n_distances[n_index++] = n_distances[d_j * n_seq + d_i];
          }
          n_distances[n_index++] = 0;
         }
//fprintf(stderr,"%d: n_index %lu, p_distances[%lu] %d\n",p,n_index -1,d_index,p_distances[d_index]);
      }
    }
    writeHdf5(n_seq,n_seq,n_seq,0,n_distances,"OUTFILE.h5");
    free(n_distances);
    free(p_distances);
  } //if rank == 0
  else
  {
    MPI_Send(distances,byProc,MPI_INT,0,123,comm);
  }
  free(distances);
  MPI_Finalize();
}

